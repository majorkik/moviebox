package com.rodion.moviebox.clean.presentation.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.clean.domain.Constants
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.person.PersonDetails
import com.rodion.moviebox.clean.presentation.extensions.displayImage
import com.rodion.moviebox.clean.presentation.ui.activities.TVDetailsActivity
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_collection.*

class TVCreditsAdapter(
    private val listMovies: List<PersonDetails.TVCredits.TVCast>
) : RecyclerView.Adapter<TVCreditsAdapter.CreditsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CreditsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_collection, parent, false)
        return CreditsViewHolder(view)
    }

    override fun getItemCount(): Int = listMovies.size

    override fun onBindViewHolder(holder: CreditsViewHolder, position: Int) {
        holder.collectionImage.displayImage(
            Constants.TMDB_POSTER_SIZE_185 + listMovies[position].posterPath,
            android.R.color.darker_gray
        )

        holder.item.setOnClickListener{
            val intent = Intent(holder.containerView.context, TVDetailsActivity::class.java)

            intent.putExtra("id", listMovies[position].id)
            intent.putExtra("title", listMovies[position].name)

            holder.containerView.context.startActivity(intent)
        }
    }

    class CreditsViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        val item: RelativeLayout = parent_layout as RelativeLayout
        val collectionImage: ImageView = collection_item_image as ImageView
    }
}