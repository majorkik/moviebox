package com.rodion.moviebox.clean.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rodion.moviebox.clean.data.models.auth.RequestToken
import com.rodion.moviebox.clean.data.models.auth.RequestTokenResponse
import com.rodion.moviebox.clean.data.models.auth.SessionResponse
import com.rodion.moviebox.clean.data.models.movie.MovieDetails
import com.rodion.moviebox.clean.data.repositories.AuthRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class AuthViewModel(val authRepository: AuthRepository): ViewModel(){
    private val parentJob = SupervisorJob()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)

    val requestToken = MutableLiveData<RequestTokenResponse>()
    val session = MutableLiveData<SessionResponse>()

    fun getRequestToken(){
        scope.launch {
            val token = authRepository.getRequestToken()
            requestToken.postValue(token)
        }
    }

    fun createSession(token: String){
        scope.launch {
            val sessionResponse = authRepository.createSession(token)
            session.postValue(sessionResponse)
        }
    }
}