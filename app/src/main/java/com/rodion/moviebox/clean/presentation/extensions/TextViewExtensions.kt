package com.rodion.moviebox.clean.presentation.extensions

import android.util.Log
import androidx.appcompat.widget.SearchView
import android.widget.TextView
import androidx.lifecycle.Lifecycle
import com.rodion.moviebox.clean.domain.DebouncingQueryTextListener
import kotlin.math.floor

fun TextView.setMovieLength(min: Int) {
    val hours: Int = floor(min / 60.0).toInt()
    val minutes: Int = min % 60
    when (hours) {
        0 -> this.text = ("${minutes}м")
        else -> this.text = ("${hours}ч ${minutes}м")
    }
}

fun SearchView.onQueryTextChange(lifecycle: Lifecycle, action: (String) -> Unit) {
    this.setOnQueryTextListener(
        DebouncingQueryTextListener(
            lifecycle
        ) { newText ->
            newText.let {
                action.invoke(newText)
                Log.i("DebounceText", newText)
            }
        })

//    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//        override fun onQueryTextSubmit(query: String): Boolean {
//            return true
//        }
//        override fun onQueryTextChange(newText: String): Boolean {
//            action.invoke(newText)
//            return true
//        }
//    })
}