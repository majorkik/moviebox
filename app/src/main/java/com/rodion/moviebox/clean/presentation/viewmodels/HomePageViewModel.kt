package com.rodion.moviebox.clean.presentation.viewmodels

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.rodion.moviebox.clean.data.models.CollectionResponse
import com.rodion.moviebox.clean.data.models.movie.MovieResponse
import com.rodion.moviebox.clean.data.models.tv.TVResponse
import com.rodion.moviebox.clean.presentation.network.TMDbApiService
import com.rodion.moviebox.clean.data.repositories.MovieRepository
import com.rodion.moviebox.clean.data.repositories.TVRepository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

class HomePageViewModel(
    val movieRepository: MovieRepository,
    val tvRepository: TVRepository
) : ViewModel() {
    private val parentJob = SupervisorJob()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)

    val popularMoviesLiveData = MutableLiveData<MutableList<CollectionResponse.CollectionItem>>()
    val popularTVsLiveData = MutableLiveData<MutableList<CollectionResponse.CollectionItem>>()
    val topRatedMoviesLiveData = MutableLiveData<MutableList<CollectionResponse.CollectionItem>>()
    val topRatedTVsLiveData = MutableLiveData<MutableList<CollectionResponse.CollectionItem>>()

    fun fetchPopularMovies(
        language: String?,
        page: Int?,
        region: String?
    ) {
        scope.launch {
            val popularMovies = movieRepository.getPopularMovies(language, page, region)
            popularMoviesLiveData.postValue(popularMovies)
        }
    }

    fun fetchPopularTVs(
        language: String?,
        page: Int?
    ) {
        scope.launch {
            val popularTVs = tvRepository.getPopularTVs(language, page)
            popularTVsLiveData.postValue(popularTVs)
        }
    }

    fun fetchTopRatedMovies(
        language: String?,
        page: Int?,
        region: String?
    ) {
        scope.launch {
            val topRatedMovies = movieRepository.getTopRatedMovies(language, page, region)
            topRatedMoviesLiveData.postValue(topRatedMovies)
        }
    }

    fun fetchTopRatedTVs(
        language: String?,
        page: Int?
    ) {
        scope.launch {
            val topRatedTVs = tvRepository.getTopRatedTVs(language, page)
            topRatedTVsLiveData.postValue(topRatedTVs)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun cancelAllRequests() = coroutineContext.cancel()
}