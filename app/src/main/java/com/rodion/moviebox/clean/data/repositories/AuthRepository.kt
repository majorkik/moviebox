package com.rodion.moviebox.clean.data.repositories

import com.rodion.moviebox.clean.data.models.auth.RequestToken
import com.rodion.moviebox.clean.data.models.auth.RequestTokenResponse
import com.rodion.moviebox.clean.data.models.auth.SessionResponse
import com.rodion.moviebox.clean.presentation.network.TMDbApiService

class AuthRepository(private val api: TMDbApiService) : BaseRepository() {
    suspend fun createSession(token: String): SessionResponse? {
        val requestToken = RequestToken(token)
        return safeApiCall(
            call = {api.createSession(requestToken).await()},
            errorMessage = "Ошибка при создании сессии"
        )
    }

    suspend fun getRequestToken(): RequestTokenResponse? {
        return safeApiCall(
            call = {
                api.getRequestToken().await()
            },
            errorMessage = "Ошибка при получении request_token"
        )
    }
}