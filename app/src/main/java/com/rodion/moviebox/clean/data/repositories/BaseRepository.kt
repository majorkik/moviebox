package com.rodion.moviebox.clean.data.repositories

import android.util.Log
import com.rodion.moviebox.clean.data.Result
import retrofit2.Response
import java.io.IOException

open class BaseRepository {
    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>, errorMessage: String): T? {
        val result: Result<T> = safeApiResult(call, errorMessage)
        var data: T? = null

        when (result) {
            is Result.Success -> data = result.data

            is Result.Error -> {
                Log.d("DataRepository", "$errorMessage & exception - ${result.exception}")
            }
        }

        return data
    }

    private suspend fun <T : Any> safeApiResult(
        call: suspend () -> Response<T>,
        errorMessage: String
    ): Result<T> {
        try {
            val response = call.invoke()
            if (response.isSuccessful) {
                return Result.Success(response.body()!!)
            }
        } catch (e: Exception) {
            Log.d("BaseRepository", e.message)
        }

        return Result.Error(IOException("Произошла ошибка при получении результата, *custom ERROR* - $errorMessage"))
    }
}