package com.rodion.moviebox.clean.presentation.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.clean.presentation.adapters.PagedMovieAdapter.OnClickListener
import com.rodion.moviebox.clean.presentation.network.NetworkState
import com.rodion.moviebox.clean.presentation.network.NetworkState.FAILED
import com.rodion.moviebox.clean.presentation.network.NetworkState.RUNNING
import com.rodion.moviebox.clean.presentation.network.NetworkState.SUCCESS
import kotlinx.android.synthetic.main.item_paging_user_network_state.view.item_paging_network_state_button as retryButton
import kotlinx.android.synthetic.main.item_paging_user_network_state.view.item_paging_network_state_progress_bar as retryProgressBar
import kotlinx.android.synthetic.main.item_paging_user_network_state.view.item_paging_network_state_title as retryTitle

class PagedMovieNetworkStateViewHolder(containerView: View) :
    RecyclerView.ViewHolder(containerView){
    fun bindTo(networkState: NetworkState?, callback: OnClickListener) {
        hideViews()
        setVisibleRightViews(networkState)
        itemView.retryButton.setOnClickListener { callback.onClickRetry() }
    }

    private fun hideViews() {
        itemView.retryButton.visibility = View.GONE
        itemView.retryProgressBar.visibility = View.GONE
        itemView.retryTitle.visibility = View.GONE
    }

    private fun setVisibleRightViews(networkState: NetworkState?) {
        when (networkState) {
            FAILED -> {
                itemView.retryButton.visibility = View.VISIBLE
                itemView.retryTitle.visibility = View.VISIBLE
            }
            RUNNING -> {
                itemView.retryProgressBar.visibility = View.VISIBLE
            }
        }
    }
}