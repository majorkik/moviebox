package com.rodion.moviebox.clean.data.di

import com.rodion.moviebox.clean.data.models.CollectionType
import com.rodion.moviebox.clean.presentation.viewmodels.AuthViewModel
import com.rodion.moviebox.clean.presentation.viewmodels.HomePageViewModel
import com.rodion.moviebox.clean.presentation.viewmodels.MovieViewModel
import com.rodion.moviebox.clean.presentation.viewmodels.PagingMovieViewModel
import com.rodion.moviebox.clean.presentation.viewmodels.PersonViewModel
import com.rodion.moviebox.clean.presentation.viewmodels.SearchableViewModel
import com.rodion.moviebox.clean.presentation.viewmodels.TVViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { HomePageViewModel(get(), get()) }
    viewModel { MovieViewModel(get()) }
    viewModel { (collectionType: CollectionType) ->
        PagingMovieViewModel(
            get(),
            get(),
            collectionType
        )
    }
    viewModel { PersonViewModel(get()) }
    viewModel { SearchableViewModel(get()) }
    viewModel { TVViewModel(get()) }
    viewModel { AuthViewModel(get()) }

}