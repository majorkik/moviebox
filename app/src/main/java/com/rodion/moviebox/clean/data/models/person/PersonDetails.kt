package com.rodion.moviebox.clean.data.models.person

import com.google.gson.annotations.SerializedName

data class PersonDetails(
    @SerializedName("birthday") val birthday: String?,
    @SerializedName("known_for_department") val knownForDepartment: String,
    @SerializedName("deathday") val deathday: String?,
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("also_known_as") val alsoKnownAs: List<String>,
    @SerializedName("gender") val gender: Int,
    @SerializedName("biography") val biography: String,
    @SerializedName("popularity") val popularity: Double,
    @SerializedName("place_of_birth") val placeOfBirth: String?,
    @SerializedName("profile_path") val profilePath: String?,
    @SerializedName("adult") val adult: Boolean,
    @SerializedName("imdb_id") val imdbId: String,
    @SerializedName("homepage") val homepage: String?,

    //append to response
    @SerializedName("movie_credits") val movieCredits: MovieCredits,
    @SerializedName("tv_credits") val tvCredits: TVCredits,
    @SerializedName("images") val images: Images,
    @SerializedName("tagged_images") val backdrops: TaggedImages
) {
    data class MovieCredits(
        @SerializedName("id") val id: Int,
        @SerializedName("cast") val cast: List<MovieCast>,
        @SerializedName("crew") val crew: List<MovieCrew>
    ) {
        data class MovieCast(
            @SerializedName("adult") val adult: Boolean,
            @SerializedName("backdrop_path") val backdropPath: String?,
            @SerializedName("character") val character: String,
            @SerializedName("credit_id") val creditId: String,
            @SerializedName("genre_ids") val genreIds: List<Int>,
            @SerializedName("id") val id: Int,
            @SerializedName("original_language") val originalLanguage: String,
            @SerializedName("original_title") val originalTitle: String,
            @SerializedName("overview") val overview: String,
            @SerializedName("popularity") val popularity: Double,
            @SerializedName("poster_path") val posterPath: String?,
            @SerializedName("release_date") val releaseDate: String,
            @SerializedName("title") val title: String,
            @SerializedName("video") val video: Boolean,
            @SerializedName("vote_average") val voteAverage: Double,
            @SerializedName("vote_count") val voteCount: Int
        )

        data class MovieCrew(
            @SerializedName("adult") val adult: Boolean,
            @SerializedName("backdrop_path") val backdropPath: String,
            @SerializedName("credit_id") val creditId: String,
            @SerializedName("department") val department: String,
            @SerializedName("genre_ids") val genreIds: List<Int>,
            @SerializedName("id") val id: Int,
            @SerializedName("job") val job: String,
            @SerializedName("original_language") val originalLanguage: String,
            @SerializedName("original_title") val originalTitle: String,
            @SerializedName("overview") val overview: String,
            @SerializedName("popularity") val popularity: Double,
            @SerializedName("poster_path") val posterPath: String?,
            @SerializedName("release_date") val releaseDate: String,
            @SerializedName("title") val title: String,
            @SerializedName("video") val video: Boolean,
            @SerializedName("vote_average") val voteAverage: Double,
            @SerializedName("vote_count") val voteCount: Int
        )
    }

    data class TVCredits(
        @SerializedName("id") val id: Int,
        @SerializedName("cast") val cast: List<TVCast>,
        @SerializedName("crew") val crew: List<TVCrew>
    ) {
        data class TVCast(
            @SerializedName("backdrop_path") val backdropPath: String?,
            @SerializedName("character") val character: String,
            @SerializedName("credit_id") val creditId: String,
            @SerializedName("episode_count") val episodeCount: Int,
            @SerializedName("first_air_date") val firstAirDate: String,
            @SerializedName("genre_ids") val genreIds: List<Int>,
            @SerializedName("id") val id: Int,
            @SerializedName("name") val name: String,
            @SerializedName("origin_country") val originCountry: List<String>,
            @SerializedName("original_language") val originalLanguage: String,
            @SerializedName("original_name") val originalName: String,
            @SerializedName("overview") val overview: String,
            @SerializedName("popularity") val popularity: Double,
            @SerializedName("poster_path") val posterPath: String?,
            @SerializedName("vote_average") val voteAverage: Double,
            @SerializedName("vote_count") val voteCount: Int
        )

        data class TVCrew(
            @SerializedName("backdrop_path") val backdropPath: String?,
            @SerializedName("credit_id") val creditId: String,
            @SerializedName("department") val department: String,
            @SerializedName("episode_count") val episodeCount: Int,
            @SerializedName("first_air_date") val firstAirDate: String,
            @SerializedName("genre_ids") val genreIds: List<Int>,
            @SerializedName("id") val id: Int,
            @SerializedName("job") val job: String,
            @SerializedName("name") val name: String,
            @SerializedName("origin_country") val originCountry: List<String>,
            @SerializedName("original_language") val originalLanguage: String,
            @SerializedName("original_name") val originalName: String,
            @SerializedName("overview") val overview: String,
            @SerializedName("popularity") val popularity: Double,
            @SerializedName("poster_path") val posterPath: String?,
            @SerializedName("vote_average") val voteAverage: Double,
            @SerializedName("vote_count") val voteCount: Int
        )
    }

    data class Images(
        @SerializedName("id") val id: Int,
        @SerializedName("profiles") val profiles: List<ImageDetails>
    ) {
        data class ImageDetails(
            @SerializedName("aspect_ratio") val aspectRatio: Double,
            @SerializedName("file_path") val filePath: String,
            @SerializedName("height") val height: Int,
            @SerializedName("iso_631_1") val iso_631_1: String?,
            @SerializedName("vote_average") val voteAverage: Double,
            @SerializedName("vote_count") val voteCount: Int,
            @SerializedName("width") val width: Int
        )
    }

    data class TaggedImages(
        @SerializedName("page") val page: Int,
        @SerializedName("total_results") val totalResults: Int,
        @SerializedName("total_pages") val totalPages: Int,
        @SerializedName("results") val results: List<ImageDetails>
    ) {
        data class ImageDetails(
            @SerializedName("aspect_ratio") val aspectRatio: Double,
            @SerializedName("file_path") val filePath: String,
            @SerializedName("height") val height: Int,
            @SerializedName("iso_631_1") val iso_631_1: String?,
            @SerializedName("vote_average") val voteAverage: Double,
            @SerializedName("vote_count") val voteCount: Int,
            @SerializedName("width") val width: Int
        )
    }
}