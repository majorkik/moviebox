package com.rodion.moviebox.clean.data.di

import com.rodion.moviebox.clean.data.repositories.AccountRepository
import com.rodion.moviebox.clean.data.repositories.AuthRepository
import com.rodion.moviebox.clean.data.repositories.MovieRepository
import com.rodion.moviebox.clean.data.repositories.PersonRepository
import com.rodion.moviebox.clean.data.repositories.SearchRepository
import com.rodion.moviebox.clean.data.repositories.TVRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory {
        MovieRepository(get())
    }
    factory {
        PersonRepository(get())
    }
    factory {
        SearchRepository(get())
    }
    factory {
        TVRepository(get())
    }
    factory {
        AuthRepository(get())
    }
    factory{
        AccountRepository(get())
    }
}