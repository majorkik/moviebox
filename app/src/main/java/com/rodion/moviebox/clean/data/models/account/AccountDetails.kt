package com.rodion.moviebox.clean.data.models.account

import com.google.gson.annotations.SerializedName

data class AccountDetails (
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("include_adult") val includeAdult: Boolean,
    @SerializedName("username") val username: String

)