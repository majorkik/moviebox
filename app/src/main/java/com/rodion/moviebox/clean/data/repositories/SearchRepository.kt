package com.rodion.moviebox.clean.data.repositories

import com.rodion.moviebox.clean.data.models.movie.MovieResponse
import com.rodion.moviebox.clean.data.models.person.PersonResponse
import com.rodion.moviebox.clean.data.models.search.MultiSearchResponse
import com.rodion.moviebox.clean.data.models.tv.TVResponse
import com.rodion.moviebox.clean.presentation.network.TMDbApiService

class SearchRepository(private val api: TMDbApiService) : BaseRepository() {

    suspend fun multiSearch(
        language: String?,
        query: String,
        page: Int?,
        includeAdult: Boolean?
    ): MultiSearchResponse? {
        return safeApiCall(
            call = {
                api.multiSearch(language, query, page, includeAdult).await()
            },
            errorMessage = "Ошибка при поиске (multi search)"
        )
    }

    suspend fun searchMovies(
        language: String?,
        query: String,
        page: Int?,
        includeAdult: Boolean?,
        region: String?,
        year: Int?,
        primaryReleaseYear: Int?
    ): MovieResponse? {
        return safeApiCall(
            call = {
                api.searchMovies(
                    language,
                    query,
                    page,
                    includeAdult,
                    region,
                    year,
                    primaryReleaseYear
                ).await()
            },
            errorMessage = "Ошибка при поиске фильмов"
        )
    }

    suspend fun searchTVs(
        language: String?,
        query: String,
        page: Int?,
        firstAirDateYear: Int?
    ): TVResponse? {
        return safeApiCall(
            call = {
                api.searchTVSeries(language, query, page, firstAirDateYear).await()
            },
            errorMessage = "Ошибка при поиске сериалов"
        )
    }

    suspend fun searchPeoples(
        language: String?,
        query: String,
        page: Int?,
        includeAdult: Boolean?,
        region: String?
    ): PersonResponse? {
        return safeApiCall(
            call = {
                api.searchPeoples(language, query, page, includeAdult, region).await()
            },
            errorMessage = "Ошибка при поиске людей"
        )
    }
}