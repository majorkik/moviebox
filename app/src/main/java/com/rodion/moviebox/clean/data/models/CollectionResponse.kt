package com.rodion.moviebox.clean.data.models

import com.google.gson.annotations.SerializedName
import com.rodion.moviebox.clean.data.models.movie.MovieResponse.Movie

data class CollectionResponse (
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: List<CollectionItem>,
    @SerializedName("total_results") val totalResults: Int,
    @SerializedName("total_pages") val totalPages: Int
){
    data class CollectionItem(
        @SerializedName("poster_path") val posterPath: String?,
        @SerializedName("adult") val adult: Boolean,
        @SerializedName("id") val id: Int,
        @SerializedName("title", alternate = ["name"]) val title: String,
        @SerializedName("popularity") val popularity: Double,
        @SerializedName("vote_count") val voteCount: Int,
        @SerializedName("vote_average") val voteAverage: Double
    )
}