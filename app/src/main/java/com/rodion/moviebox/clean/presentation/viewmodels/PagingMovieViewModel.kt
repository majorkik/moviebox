package com.rodion.moviebox.clean.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.rodion.moviebox.clean.data.models.CollectionType
import com.rodion.moviebox.clean.data.repositories.MovieRepository
import com.rodion.moviebox.clean.data.repositories.TVRepository
import com.rodion.moviebox.clean.domain.pagination.MovieDataSourceFactory
import com.rodion.moviebox.clean.presentation.network.NetworkState

class PagingMovieViewModel(movieRepository: MovieRepository, tvRepository: TVRepository, collectionType: CollectionType) : BaseViewModel() {
    private val movieDataSource = MovieDataSourceFactory(
        repositoryMovie = movieRepository,
        repositoryTV = tvRepository,
        collectionType = collectionType,
        scope = ioScope
    )

    val movieResults = LivePagedListBuilder(movieDataSource, pagedConfig()).build()

    val networkState: LiveData<NetworkState>? =
        Transformations.switchMap(movieDataSource.source) { it.getNetworkState() }

    fun fetchItems(query: String) {
        movieDataSource.update()
    }

    private fun pagedConfig() = PagedList.Config.Builder()
        .setInitialLoadSizeHint(5)
        .setEnablePlaceholders(false)
        .setPageSize(20)
        .build()

    fun refreshFailedRequest() = movieDataSource.getSource()?.retryFailedQuery()

    fun refreshAllList() = movieDataSource.getSource()?.refresh()
}