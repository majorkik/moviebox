package com.rodion.moviebox.clean.presentation.ui.fragments

import android.app.Activity
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.CollectionType.POPULAR_MOVIE
import com.rodion.moviebox.clean.data.models.CollectionType.POPULAR_TV
import com.rodion.moviebox.clean.data.models.CollectionType.TOP_RATED_MOVIE
import com.rodion.moviebox.clean.data.models.CollectionType.TOP_RATED_TV
import com.rodion.moviebox.clean.data.models.MediaType
import com.rodion.moviebox.clean.presentation.adapters.CollectionAdapter
import com.rodion.moviebox.clean.presentation.adapters.MovieCollectionAdapter
import com.rodion.moviebox.clean.presentation.adapters.TVCollectionAdapter
import com.rodion.moviebox.clean.presentation.viewmodels.HomePageViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {
    private val TAG = "HomeFragment"

    private val homePageViewModel: HomePageViewModel by viewModel()

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_home, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fetchData(homePageViewModel)

        setObservers(homePageViewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        popular_movie_card.setOnClickListener {
            fragmentManager?.beginTransaction()
                ?.replace(R.id.relativelayout, MovieFragment(POPULAR_MOVIE))
                ?.commit()
        }
        popular_tv_card.setOnClickListener {
            fragmentManager?.beginTransaction()
                ?.replace(R.id.relativelayout, TVFragment(POPULAR_TV))
                ?.commit()
        }
        top_rated_movie_card.setOnClickListener {
            fragmentManager?.beginTransaction()
                ?.replace(R.id.relativelayout, MovieFragment(TOP_RATED_MOVIE))
                ?.commit()
        }
        top_rated_tv_card.setOnClickListener {
            fragmentManager?.beginTransaction()
                ?.replace(R.id.relativelayout, TVFragment(TOP_RATED_TV))
                ?.commit()
        }

    }

    private fun setLayoutManagerForRecyclerView(
        rootView: Activity,
        idRecyclerView: Int
    ): RecyclerView {
        val recyclerView: RecyclerView = rootView.findViewById(idRecyclerView)
        recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        return recyclerView
    }

    private fun fetchData(homePageViewModel: HomePageViewModel) {
        homePageViewModel.fetchPopularMovies("ru-RU,en-US,null", 1, null)
        homePageViewModel.fetchPopularTVs("ru-RU,en-US,null", 1)
        homePageViewModel.fetchTopRatedMovies("ru-RU,en-US,null", 1, null)
        homePageViewModel.fetchTopRatedTVs("ru-RU,en-US,null", 1)
    }

    private fun setObservers(homePageViewModel: HomePageViewModel) {
        activity?.let {
            homePageViewModel.popularMoviesLiveData.observe(this, Observer { movies ->
                if (movies != null) {
                    setLayoutManagerForRecyclerView(it, R.id.recyclerview_popular_movie).adapter =
                        CollectionAdapter(movies, MediaType.MOVIE)
                }else{
                    no_internet_popular_movie.visibility = LinearLayout.VISIBLE
                }
            })

            homePageViewModel.popularTVsLiveData.observe(this, Observer { tvs ->
                if(tvs != null) {
                    setLayoutManagerForRecyclerView(it, R.id.recyclerview_popular_tv).adapter =
                        CollectionAdapter(tvs,MediaType.TV)
                }else{
                    no_internet_popular_tv.visibility = LinearLayout.VISIBLE
                }
            })

            homePageViewModel.topRatedMoviesLiveData.observe(this, Observer { movies ->
                if(movies != null) {
                    setLayoutManagerForRecyclerView(it, R.id.recyclerview_top_rated_movie).adapter =
                        CollectionAdapter(movies, MediaType.MOVIE)
                }else {
                    no_internet_top_rated_movie.visibility = LinearLayout.VISIBLE
                }
            })

            homePageViewModel.topRatedTVsLiveData.observe(this, Observer { tvs ->
                if(tvs != null) {
                    setLayoutManagerForRecyclerView(it, R.id.recyclerview_top_rated_tv).adapter =
                        CollectionAdapter(tvs, MediaType.TV)
                }else{
                    no_internet_top_rated_tv.visibility = LinearLayout.VISIBLE
                }
            })
        }
    }
}