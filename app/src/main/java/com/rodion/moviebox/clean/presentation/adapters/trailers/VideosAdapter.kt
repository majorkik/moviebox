package com.rodion.moviebox.clean.presentation.adapters.trailers

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.clean.domain.Constants
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.Video
import com.rodion.moviebox.clean.presentation.adapters.trailers.VideosAdapter.VideoViewHolder
import com.rodion.moviebox.clean.presentation.extensions.displayImage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_video.*


class VideosAdapter(
    private val listMovies: List<Video>
) : RecyclerView.Adapter<VideoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_video, parent, false)

        return VideoViewHolder(
            view
        )
    }

    override fun getItemCount(): Int = listMovies.size

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        val videoKey: String = listMovies[position].key

        holder.collectionImage.displayImage(Constants.YOUTUBE_IMAGE_LINK + videoKey + Constants.YOUTUBE_SIZE_MQ)

        holder.nameVideo.text = listMovies[position].name

        holder.item.setOnClickListener {
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$videoKey"))
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=$videoKey")
            )
            try {
                holder.containerView.context.startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                holder.containerView.context.startActivity(webIntent)
            }
        }

    }

    class VideoViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        val item: LinearLayout = parent_layout as LinearLayout
        val collectionImage: ImageView = image_video as ImageView
        val nameVideo: TextView = name_trailer as TextView
    }
}