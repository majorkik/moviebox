package com.rodion.moviebox.clean.domain

import com.rodion.moviebox.BuildConfig

object Constants {
    const val TMDB_BASE_URL = "https://api.themoviedb.org/3/"

    const val TMDB_AUTH_URL = "https://www.themoviedb.org/authenticate/"

    const val TMDB_POSTER_SIZE_92 = "https://image.tmdb.org/t/p/w92"
    const val TMDB_POSTER_SIZE_154 = "https://image.tmdb.org/t/p/w154"
    const val TMDB_POSTER_SIZE_185 = "https://image.tmdb.org/t/p/w185"
    const val TMDB_POSTER_SIZE_342 = "https://image.tmdb.org/t/p/w342"
    const val TMDB_POSTER_SIZE_500 = "https://image.tmdb.org/t/p/w500"
    const val TMDB_POSTER_SIZE_780 = "https://image.tmdb.org/t/p/w780"

    const val TMDB_BACKDROP_SIZE_300 = "https://image.tmdb.org/t/p/w300"
    const val TMDB_BACKDROP_SIZE_780 = "https://image.tmdb.org/t/p/w780"
    const val TMDB_BACKDROP_SIZE_1280 = "https://image.tmdb.org/t/p/w1280"

    const val TMDB_PROFILE_SIZE_45 = "https://image.tmdb.org/t/p/w45"
    const val TMDB_PROFILE_SIZE_185 = "https://image.tmdb.org/t/p/w185"
    const val TMDB_PROFILE_SIZE_632 = "https://image.tmdb.org/t/p/H632"

    const val TMDB_LOGO_SIZE_45 = "https://image.tmdb.org/t/p/w45"
    const val TMDB_LOGO_SIZE_92 = "https://image.tmdb.org/t/p/w92"
    const val TMDB_LOGO_SIZE_154 = "https://image.tmdb.org/t/p/w154"
    const val TMDB_LOGO_SIZE_185 = "https://image.tmdb.org/t/p/w185"
    const val TMDB_LOGO_SIZE_300 = "https://image.tmdb.org/t/p/w300"
    const val TMDB_LOGO_SIZE_500 = "https://image.tmdb.org/t/p/w500"

    const val TMDB_STILL_SIZE_92 = "https://image.tmdb.org/t/p/w92"
    const val TMDB_STILL_SIZE_185 = "https://image.tmdb.org/t/p/w185"
    const val TMDB_STILL_SIZE_300 = "https://image.tmdb.org/t/p/w300"

    const val TMDB_SIZE_ORIGINAL = "original"

    var TMDB_API_KEY = BuildConfig.TMDB_API_KEY

    //youtube
    const val YOUTUBE_IMAGE_LINK = "https://img.youtube.com/vi/"

    const val YOUTUBE_SIZE_DEFAULT = "/default.jpg"
    const val YOUTUBE_SIZE_HQ = "/hqdefault.jpg"
    const val YOUTUBE_SIZE_MQ = "/mqdefault.jpg"
    const val YOUTUBE_SIZE_SQ = "/sddefault.jpg"
    const val YOUTUBE_SIZE_MAXRES = "/maxresdefault.jpg"
}