package com.rodion.moviebox.clean.presentation.viewmodels

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.rodion.moviebox.clean.data.models.movie.MovieDetails
import com.rodion.moviebox.clean.presentation.network.TMDbApiService
import com.rodion.moviebox.clean.data.repositories.MovieRepository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class MovieViewModel(
    val movieRepository: MovieRepository) : ViewModel() {
    private val parentJob = Job()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)

    var movieDetailsLiveData = MutableLiveData<MovieDetails>()

    fun fetchMovieDetails(
        movieId: Int,
        language: String?,
        appendToResponse: String?,
        imageLanguages: String?
    ) {
        scope.launch {
            val movieDetails =
                movieRepository.getMovieById(movieId, language, appendToResponse, imageLanguages)
            movieDetailsLiveData.postValue(movieDetails)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun cancelAllRequests() = coroutineContext.cancel()
}