package com.rodion.moviebox.clean.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.tv.TVSeasonDetails
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_tv_season.text_season_name

class TVSeriesAdapter(private val series: List<TVSeasonDetails.Episode>):
    RecyclerView.Adapter<TVSeriesAdapter.TVSeriesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TVSeriesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_tv_season, parent, false)

        return TVSeriesViewHolder(view)
    }

    override fun getItemCount() = series.size

    override fun onBindViewHolder(holder: TVSeriesViewHolder, position: Int) {
        holder.title.text = series[position].name
    }

    class TVSeriesViewHolder(override var containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer{
        var title = text_season_name as TextView
    }

}