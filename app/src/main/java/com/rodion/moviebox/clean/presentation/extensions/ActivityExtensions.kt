package com.rodion.moviebox.clean.presentation.extensions

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.rodion.moviebox.R

fun AppCompatActivity.setStatusBarSupport() {
    if (Build.VERSION.SDK_INT in 19..20) setWindowFlag(
        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
        true
    )
    if (Build.VERSION.SDK_INT >= 19) window.decorView.systemUiVisibility =
        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    if (Build.VERSION.SDK_INT >= 21) {
        setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
        window.statusBarColor = Color.TRANSPARENT
    }
}

private fun AppCompatActivity.setWindowFlag(bits: Int, on: Boolean) {
    val win = window
    val winParams = win.attributes
    when {
        on -> winParams.flags = winParams.flags or bits
        else -> winParams.flags = winParams.flags and bits.inv()
    }
    win.attributes = winParams
}

fun AppCompatActivity.setTitleCollapsingToolbar(extras: Bundle?){
    val collapsingToolbarLayout =
        findViewById<View>(R.id.collapsing_toolbar) as CollapsingToolbarLayout

    val appBarLayout = findViewById<View>(R.id.app_bar) as AppBarLayout
    appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
        var isShow = true
        var scrollRange = -1

        override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
            if (scrollRange == -1) {
                scrollRange = appBarLayout.totalScrollRange
            }
            if (scrollRange + verticalOffset == 0) {
                collapsingToolbarLayout.title = extras?.getString("title")
                isShow = true
            } else if (isShow) {
                collapsingToolbarLayout.title = " "
                isShow = false
            }
        }
    })
}

fun AppCompatActivity.setToolbarWithBackButton(idToolbar: Int){
    val toolbar: Toolbar = findViewById(idToolbar)
    setSupportActionBar(toolbar)

    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
}