package com.rodion.moviebox.clean.data.models

enum class MediaType(val value: String) {
    MOVIE("movie"),
    TV("tv"),
    PERSON("person")
}