package com.rodion.moviebox.clean.data.models.person

import com.google.gson.annotations.SerializedName

data class PersonPostersResponse(
    @SerializedName("profiles") val profiles: List<ImageDetails>
) {
    data class ImageDetails (
        @SerializedName("aspect_ratio") val aspectRatio: Double,
        @SerializedName("file_path") val filePath: String,
        @SerializedName("height") val height: Int,
        @SerializedName("iso_631_1") val iso_631_1: String?,
        @SerializedName("vote_average") val voteAverage: Double,
        @SerializedName("vote_count") val voteCount: Int,
        @SerializedName("width") val width: Int
    )
}