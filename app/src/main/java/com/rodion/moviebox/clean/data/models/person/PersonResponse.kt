package com.rodion.moviebox.clean.data.models.person

import com.google.gson.annotations.SerializedName

data class PersonResponse(
    @SerializedName("page") val page: Int,
    @SerializedName("total_results") val totalResults: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("results") val results: List<Person>
){
    data class Person(
        @SerializedName("id") val id: Int,
        @SerializedName("profile_path") val profilePath: String?,
        @SerializedName("name") val name: String,
        @SerializedName("popularity") val popularity: Double
    )
}