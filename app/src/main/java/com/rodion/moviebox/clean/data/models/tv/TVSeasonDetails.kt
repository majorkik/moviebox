package com.rodion.moviebox.clean.data.models.tv

import com.google.gson.annotations.SerializedName
import com.rodion.moviebox.clean.data.models.Cast
import com.rodion.moviebox.clean.data.models.Crew

data class TVSeasonDetails(
    @SerializedName("_id") val secondaryId: String,
    @SerializedName("air_date") val airDate: String,
    @SerializedName("episodes") val episodes: List<Episode>,
    @SerializedName("name") val name: String,
    @SerializedName("overview") val overview: String,
    @SerializedName("id") val id: Int,
    @SerializedName("poster_path") val posterPath: String?,
    @SerializedName("season_number") val seasonNumber: Int
) {
    data class Episode(
        @SerializedName("air_date") val airDate: String,
        @SerializedName("crew") val crew: List<Crew>,
        @SerializedName("episode_number") val episodeNumber: Int,
        @SerializedName("guest_stars") val casts: List<Cast>,
        @SerializedName("name") val name: String,
        @SerializedName("overview") val overview: String,
        @SerializedName("id") val id: Int,
        @SerializedName("production_code") val productionCode: String?,
        @SerializedName("season_number") val seasonNumber: Int,
        @SerializedName("still_path") val stillPath: String?,
        @SerializedName("vote_average") val voteAverage: Double,
        @SerializedName("vote_count") val voteCount: Int
    )
}