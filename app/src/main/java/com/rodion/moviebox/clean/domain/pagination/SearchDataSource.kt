package com.rodion.moviebox.clean.domain.pagination

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.rodion.moviebox.clean.data.models.search.MultiSearchResponse
import com.rodion.moviebox.clean.data.models.search.MultiSearchResponse.MultiSearchItem
import com.rodion.moviebox.clean.data.repositories.SearchRepository
import com.rodion.moviebox.clean.presentation.network.NetworkState
import com.rodion.moviebox.clean.presentation.network.NetworkState.FAILED
import com.rodion.moviebox.clean.presentation.network.NetworkState.RUNNING
import com.rodion.moviebox.clean.presentation.network.NetworkState.SUCCESS
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SearchDataSource(
    private val repository: SearchRepository,
    private val query: String,
    private val scope: CoroutineScope
) : PageKeyedDataSource<Int, MultiSearchItem>() {

    private var supervisorJob = SupervisorJob()
    private val networkState = MutableLiveData<NetworkState>()
    private var retryQuery: (() -> Any)? = null

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, MultiSearchItem>
    ) {
        retryQuery = { loadInitial(params, callback) }
        executeQuery(1, params.requestedLoadSize) {
            callback.onResult(it, null, 2)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, MultiSearchItem>) {
        val page = params.key
        retryQuery = { loadAfter(params, callback) }
        executeQuery(page, params.requestedLoadSize) {
            callback.onResult(it, page + 1)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, MultiSearchItem>) {

    }

    private fun executeQuery(page: Int, perPage: Int, callback: (List<MultiSearchItem>) -> Unit) {
        networkState.postValue(RUNNING)
        scope.launch(getJobErrorHandler() + supervisorJob) {
            delay(200)
            val searchResults = repository.multiSearch("ru", query, page, false)
            retryQuery = null
            networkState.postValue(SUCCESS)
            searchResults?.results?.let { callback(it) }
        }
    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        Log.e(SearchDataSource::class.java.simpleName, "Ошибка: $e")
        networkState.postValue(FAILED)
    }

    override fun invalidate() {
        super.invalidate()
        supervisorJob.cancelChildren()
    }

    fun getNetworkState(): LiveData<NetworkState> = networkState

    fun refresh() = this.invalidate()

    fun retryFailedQuery() {
        val previousQuery = retryQuery
        retryQuery = null
        previousQuery?.invoke()
    }

}