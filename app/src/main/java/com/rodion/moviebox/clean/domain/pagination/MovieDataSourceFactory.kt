package com.rodion.moviebox.clean.domain.pagination

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.rodion.moviebox.clean.data.models.CollectionResponse
import com.rodion.moviebox.clean.data.models.CollectionType
import com.rodion.moviebox.clean.data.models.movie.MovieResponse
import com.rodion.moviebox.clean.data.models.search.MultiSearchResponse.MultiSearchItem
import com.rodion.moviebox.clean.data.repositories.MovieRepository
import com.rodion.moviebox.clean.data.repositories.SearchRepository
import com.rodion.moviebox.clean.data.repositories.TVRepository
import kotlinx.coroutines.CoroutineScope

class MovieDataSourceFactory(
    private val repositoryMovie: MovieRepository,
    private val repositoryTV: TVRepository,
    private val collectionType: CollectionType,
    private val scope: CoroutineScope
) : DataSource.Factory<Int, CollectionResponse.CollectionItem>() {
    val source = MutableLiveData<MovieDataSource>()

    override fun create(): DataSource<Int, CollectionResponse.CollectionItem> {
        val source = MovieDataSource(repositoryMovie, repositoryTV, collectionType, scope)
        this.source.postValue(source)
        return source
    }

    fun getSource() = source.value

    fun update(){
        getSource()?.refresh()
    }
}