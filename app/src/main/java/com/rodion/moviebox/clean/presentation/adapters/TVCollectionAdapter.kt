package com.rodion.moviebox.clean.presentation.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.clean.domain.Constants
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.tv.TVResponse
import com.rodion.moviebox.clean.presentation.extensions.displayImage
import com.rodion.moviebox.clean.presentation.ui.activities.TVDetailsActivity
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_collection.*


class TVCollectionAdapter(
    private val listTV: List<TVResponse.TV>
) : RecyclerView.Adapter<TVCollectionAdapter.CollectionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollectionViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_collection, parent, false)

        return CollectionViewHolder(view)
    }

    override fun getItemCount(): Int = listTV.size

    override fun onBindViewHolder(holder: CollectionViewHolder, position: Int) {
        holder.collectionImage.displayImage(Constants.TMDB_POSTER_SIZE_92 + listTV[position].posterPath)

        holder.item.setOnClickListener {
            val intent = Intent(holder.containerView.context, TVDetailsActivity::class.java)

            intent.putExtra("id", listTV[position].id)
            intent.putExtra("title", listTV[position].name)

            holder.containerView.context.startActivity(intent)
        }
    }

    class CollectionViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        val item: RelativeLayout = parent_layout as RelativeLayout
        val collectionImage: ImageView = collection_item_image as ImageView
    }
}