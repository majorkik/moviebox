package com.rodion.moviebox.clean.data.models.tv

import com.google.gson.annotations.SerializedName
import com.rodion.moviebox.clean.data.models.tv.TVResponse.TV

data class TVEpisodeResponse(
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: List<TVEpisodeDetails>,
    @SerializedName("total_results") val totalResults: Int,
    @SerializedName("total_pages") val totalPages: Int
)