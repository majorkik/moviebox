package com.rodion.moviebox.clean.data.di

val appComponent = listOf(networkModule, viewModelModule, repositoryModule)