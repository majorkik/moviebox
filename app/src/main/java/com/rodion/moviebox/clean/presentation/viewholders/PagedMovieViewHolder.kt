package com.rodion.moviebox.clean.presentation.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.clean.data.models.CollectionResponse
import com.rodion.moviebox.clean.data.models.movie.MovieResponse
import com.rodion.moviebox.clean.domain.Constants
import com.rodion.moviebox.clean.presentation.extensions.displayImage
import kotlinx.android.synthetic.main.item_movie_card.view.image_item_movie as image

class PagedMovieViewHolder(val parent: View) : RecyclerView.ViewHolder(parent) {
    fun bindTo(movie: CollectionResponse.CollectionItem?) {
        movie?.posterPath?.let { path ->
            itemView.image.displayImage(Constants.TMDB_POSTER_SIZE_92 + path)
        }
    }
}