package com.rodion.moviebox.clean.presentation.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.CollectionResponse
import com.rodion.moviebox.clean.data.models.MediaType
import com.rodion.moviebox.clean.data.models.MediaType.MOVIE
import com.rodion.moviebox.clean.data.models.MediaType.PERSON
import com.rodion.moviebox.clean.data.models.MediaType.TV
import com.rodion.moviebox.clean.presentation.network.NetworkState
import com.rodion.moviebox.clean.presentation.network.NetworkState.SUCCESS
import com.rodion.moviebox.clean.presentation.ui.activities.MovieDetailsActivity
import com.rodion.moviebox.clean.presentation.ui.activities.PersonDetailsActivity
import com.rodion.moviebox.clean.presentation.ui.activities.TVDetailsActivity
import com.rodion.moviebox.clean.presentation.viewholders.PagedMovieNetworkStateViewHolder
import com.rodion.moviebox.clean.presentation.viewholders.PagedMovieViewHolder

class PagedMovieAdapter(private val mediaType: MediaType, private val callback: OnClickListener) :
    PagedListAdapter<CollectionResponse.CollectionItem, ViewHolder>(
        diffCallback
    ) {

    private var networkState: NetworkState? = null

    interface OnClickListener {
        fun onClickRetry()
        fun whenListIsUpdated(size: Int, networkState: NetworkState?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.item_movie_card -> PagedMovieViewHolder(
                view
            )
            R.layout.item_paging_user_network_state -> PagedMovieNetworkStateViewHolder(
                view
            )
            else -> throw IllegalArgumentException("Неизвестный тип view: $viewType")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_movie_card -> {
                (holder as PagedMovieViewHolder).bindTo(getItem(position))

                holder.itemView.setOnClickListener {
                    getItem(position)?.let { movie ->
                        val intent = Intent(holder.parent.context, changeScreen())

                        intent.putExtra("id", movie.id)
                        intent.putExtra("title", movie.title)

                        holder.parent.context.startActivity(intent)
                    }
                }
            }
            R.layout.item_paging_user_network_state -> {
                (holder as PagedMovieNetworkStateViewHolder).bindTo(
                    networkState,
                    callback
                )
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            R.layout.item_paging_user_network_state
        } else {
            R.layout.item_movie_card
        }
    }

    override fun getItemCount(): Int {
        this.callback.whenListIsUpdated(super.getItemCount(), this.networkState)
        return super.getItemCount()
    }

    private fun hasExtraRow() = networkState != null && networkState != SUCCESS

    fun updateNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    private fun changeScreen(): Class<*> {
        return when (mediaType) {
            MOVIE -> {
                MovieDetailsActivity::class.java
            }
            TV -> {
                TVDetailsActivity::class.java
            }
            PERSON -> {
                PersonDetailsActivity::class.java
            }
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<CollectionResponse.CollectionItem>() {
            override fun areItemsTheSame(oldItem: CollectionResponse.CollectionItem, newItem: CollectionResponse.CollectionItem): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(oldItem: CollectionResponse.CollectionItem, newItem: CollectionResponse.CollectionItem): Boolean =
                oldItem == newItem
        }
    }
}