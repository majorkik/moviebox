package com.rodion.moviebox.clean.presentation.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.Tab
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.CollectionType.POPULAR_MOVIE
import com.rodion.moviebox.clean.data.models.CollectionType.POPULAR_TV
import com.rodion.moviebox.clean.data.models.CollectionType.TOP_RATED_MOVIE
import com.rodion.moviebox.clean.data.models.CollectionType.TOP_RATED_TV
import com.rodion.moviebox.clean.presentation.adapters.tabs.MovieCollectionsPageAdapter
import com.rodion.moviebox.clean.presentation.extensions.setStatusBarSupport
import com.rodion.moviebox.clean.presentation.ui.fragments.MovieFragment
import com.rodion.moviebox.clean.presentation.ui.fragments.TVFragment
import kotlinx.android.synthetic.main.activity_movie_collections.*

class MovieCollectionsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_collections)

        setSupportActionBar(toolbar)

        //setStatusBarSupport()

        configureTabLayout()
    }

    private fun configureTabLayout() {
        val adapter = MovieCollectionsPageAdapter(supportFragmentManager)

        adapter.addFragment(MovieFragment(POPULAR_MOVIE), "Популярные")
        adapter.addFragment(MovieFragment(TOP_RATED_MOVIE), "Самые популярные")
        view_pager.adapter = adapter

        if(adapter.count < 3) {
            tab_layout.tabMode = TabLayout.MODE_FIXED
        }else{
            tab_layout.tabMode = TabLayout.MODE_SCROLLABLE
        }

        tab_layout.setupWithViewPager(view_pager)
        tab_layout.getTabAt(1)?.select()
    }
}

