package com.rodion.moviebox.clean.presentation.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.clean.domain.Constants
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.movie.MovieResponse
import com.rodion.moviebox.clean.presentation.extensions.displayImage
import com.rodion.moviebox.clean.presentation.ui.activities.MovieDetailsActivity
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_movie_card.*

class ListVerticalAdapter(
    private var items: MutableList<MovieResponse.Movie>
) : RecyclerView.Adapter<ListVerticalAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movie_card, parent, false)

        return ListViewHolder(view)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.poster.displayImage(Constants.TMDB_POSTER_SIZE_92 + items[position].posterPath)

        holder.item.setOnClickListener {
            val intent = Intent(holder.containerView.context, MovieDetailsActivity::class.java)

            intent.putExtra("id", items[position].id)
            intent.putExtra("title", items[position].title)


            holder.containerView.context.startActivity(intent)
        }
    }

    fun setItems(items: List<MovieResponse.Movie>){
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    fun addItems(items: List<MovieResponse.Movie>){
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    class ListViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        val item: RelativeLayout = parent_layout as RelativeLayout
        val poster: ImageView = image_item_movie as ImageView
    }
}