package com.rodion.moviebox.clean.data.repositories

import com.rodion.moviebox.clean.data.models.CollectionResponse
import com.rodion.moviebox.clean.data.models.genre.Genre
import com.rodion.moviebox.clean.data.models.tv.TVDetails
import com.rodion.moviebox.clean.data.models.tv.TVResponse
import com.rodion.moviebox.clean.presentation.network.TMDbApiService

class TVRepository(private val api: TMDbApiService) : BaseRepository() {

    suspend fun getTVById(
        tvId: Int,
        language: String?,
        appendToResponse: String?,
        imageLanguages: String?
    ): TVDetails? {

        return safeApiCall(
            call = { api.getTVById(tvId, language, appendToResponse, imageLanguages).await() },
            errorMessage = "Ошибка при получении информации о сериале"
        )
    }

    suspend fun getPopularTVs(
        language: String?,
        page: Int?
    ): MutableList<CollectionResponse.CollectionItem>? {
        val tvResponse = safeApiCall(
            call = { api.getPopularTVs(language, page).await() },
            errorMessage = "Ошибка при получении популярных сериалов"
        )

        return tvResponse?.results?.toMutableList()
    }

    suspend fun getTopRatedTVs(
        language: String?,
        page: Int?
    ): MutableList<CollectionResponse.CollectionItem>? {
        val tvResponse = safeApiCall(
            call = { api.getTopRatedTVs(language, page).await() },
            errorMessage = "Ошибка при получении самых популярных сериалов"
        )

        return tvResponse?.results?.toMutableList()
    }

    suspend fun searchTVs(
        language: String?,
        query: String,
        page: Int?,
        firstAirDateYear: Int?
    ): MutableList<TVResponse.TV>? {
        val tvResponse = safeApiCall(
            call = { api.searchTVSeries(language, query, page, firstAirDateYear).await() },
            errorMessage = "Ошибка при поиске сериалов"
        )

        return tvResponse?.results?.toMutableList()
    }

    suspend fun getTVGenres(language: String?): MutableList<Genre>? {
        val tvResponse = safeApiCall(
            call = { api.getTVGenres(language).await() },
            errorMessage = "Ошбика при получении списка жанров для сериалов"
        )

        return tvResponse?.genres?.toMutableList()
    }
}