package com.rodion.moviebox.clean.presentation.ui.fragments

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.presentation.adapters.SearchAdapter
import com.rodion.moviebox.clean.presentation.extensions.onQueryTextChange
import com.rodion.moviebox.clean.presentation.network.NetworkState
import com.rodion.moviebox.clean.presentation.network.NetworkState.FAILED
import com.rodion.moviebox.clean.presentation.network.NetworkState.RUNNING
import com.rodion.moviebox.clean.presentation.network.NetworkState.SUCCESS
import com.rodion.moviebox.clean.presentation.viewmodels.SearchableViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlinx.android.synthetic.main.fragment_search_page.fragment_search_list as recyclerView
import kotlinx.android.synthetic.main.fragment_search_page.movie_empty_list_button as emptyListButton
import kotlinx.android.synthetic.main.fragment_search_page.movie_empty_list_image as emptyListImage
import kotlinx.android.synthetic.main.fragment_search_page.movie_empty_list_title as emptyListTitle
import kotlinx.android.synthetic.main.fragment_search_page.movie_empty_list_progressbar as progressBar

class SearchableFragment: BasePagedFragment(), SearchAdapter.OnClickListener {
    private val viewModel: SearchableViewModel by viewModel()
    private lateinit var adapter: SearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun getLayoutId() = R.layout.fragment_search_page

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureRecyclerView()
        configureObservables()
        configureOnClick()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        configureMenu(menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onClickRetry() {
        viewModel.refreshFailedRequest()
    }

    override fun whenListIsUpdated(size: Int, networkState: NetworkState?) {
        updateUIWhenLoading(size, networkState)
        updateUIWhenEmptyList(size, networkState)
    }

    private fun configureOnClick() {
        emptyListButton.setOnClickListener{viewModel.refreshAllList()}
    }

    private fun configureMenu(menu: Menu) {
        val searchMenuItem = menu.findItem(R.id.action_search)
        val possibleExistingQuery = viewModel.getCurrentQuery()
        val searchView = searchMenuItem.actionView as SearchView
        if (possibleExistingQuery.isNotEmpty()) {
            searchMenuItem.expandActionView()
            searchView.setQuery(possibleExistingQuery, false)
            searchView.clearFocus()
        }
        searchView.onQueryTextChange(this@SearchableFragment.lifecycle) { viewModel.fetchItems(it) }
    }

    private fun configureRecyclerView() {
        adapter = SearchAdapter(this)
        recyclerView.adapter = adapter
    }

    private fun configureObservables() {
        viewModel.networkState?.observe(this, Observer { adapter.updateNetworkState(it) })
        viewModel.searchResults.observe(this, Observer { adapter.submitList(it) })
    }

    private fun updateUIWhenEmptyList(size: Int, networkState: NetworkState?) {
        emptyListImage.visibility = View.GONE
        emptyListButton.visibility = View.GONE
        emptyListTitle.visibility = View.GONE
        if (size == 0) {
            when (networkState) {
                SUCCESS -> {
                    emptyListTitle.text = getString(R.string.no_result_found)
                    emptyListImage.visibility = View.VISIBLE
                    emptyListTitle.visibility = View.VISIBLE
                    emptyListButton.visibility = View.GONE
                }
                FAILED -> {
                    emptyListTitle.text = getString(R.string.technical_error)
                    emptyListImage.visibility = View.VISIBLE
                    emptyListTitle.visibility = View.VISIBLE
                    emptyListButton.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun updateUIWhenLoading(size: Int, networkState: NetworkState?) {
        progressBar.visibility =
            if (size == 0 && networkState == RUNNING) View.VISIBLE else View.GONE
    }

}
