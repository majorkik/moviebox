package com.rodion.moviebox.clean.data.models.search

import com.google.gson.annotations.SerializedName

data class MultiSearchResponse(
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: List<MultiSearchItem>,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("total_results") val totalResults: Int
) {
    data class MultiSearchItem(
        @SerializedName("id") val id: Int,

        //posterPath or profilePath
        @SerializedName("poster_path") val posterPath: String?,
        @SerializedName("profile_path") val profilePath: String?,

        @SerializedName("media_type") val mediaType: String,

        //name or title
        @SerializedName("name") val name: String?,
        @SerializedName("title") val title: String?

    )
}