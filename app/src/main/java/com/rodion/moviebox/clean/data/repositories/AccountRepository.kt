package com.rodion.moviebox.clean.data.repositories

import com.rodion.moviebox.clean.data.models.account.AccountDetails
import com.rodion.moviebox.clean.data.models.account.RequestAddToWatchlist
import com.rodion.moviebox.clean.data.models.account.RequestMarkAsFavorite
import com.rodion.moviebox.clean.data.models.account.ResponseAPI
import com.rodion.moviebox.clean.data.models.movie.MovieResponse
import com.rodion.moviebox.clean.data.models.tv.TVEpisodeResponse
import com.rodion.moviebox.clean.data.models.tv.TVResponse
import com.rodion.moviebox.clean.presentation.network.TMDbApiService

class AccountRepository(private val api: TMDbApiService) : BaseRepository() {
    suspend fun getAccountDetails(sessionId: String): AccountDetails? {
        return safeApiCall(
            call = {
                api.getAccountDetails(sessionId).await()
            },
            errorMessage = "Ошибка при получении информации о пользователе"
        )
    }

    suspend fun getFavoriteMovies(
        language: String?,
        sessionId: String,
        sortBy: String?,
        page: Int?
    ): MovieResponse? {
        return safeApiCall(
            call = {
                api.getFavoriteMovies(language, sessionId, sortBy, page).await()
            }, errorMessage = "Ошибка при получении понравившихся фильмов"
        )
    }

    suspend fun getFavoriteTVs(
        language: String?,
        sessionId: String,
        sortBy: String?,
        page: Int?
    ): TVResponse? {
        return safeApiCall(
            call = {
                api.getFavoriteTVs(language, sessionId, sortBy, page).await()
            }, errorMessage = "Ошибка при получении понравившихся сериалов"
        )
    }

    suspend fun markIsFavorite(
        requestMarkAsFavorite: RequestMarkAsFavorite,
        sessionId: String
    ): ResponseAPI? {
        return safeApiCall(
            call = {
                api.markIsFavorite(requestMarkAsFavorite, sessionId).await()
            },
            errorMessage = "Ошибка, неудалось установить флаг 'Нарвится'" +
                    " со значением: ${requestMarkAsFavorite.favorite} " +
                    "для типа: ${requestMarkAsFavorite.mediaType}" +
                    " c id: ${requestMarkAsFavorite.mediaId} "
        )
    }

    suspend fun getRatedMovies(
        language: String?,
        sessionId: String,
        sortBy: String?,
        page: Int?
    ): MovieResponse? {
        return safeApiCall(
            call = {
                api.getRatedMovies(language, sessionId, sortBy, page).await()
            }, errorMessage = "Ошибка при получении оцененных фильмов"
        )
    }

    suspend fun getRatedTVs(
        language: String?,
        sessionId: String,
        sortBy: String?,
        page: Int?
    ): TVResponse? {
        return safeApiCall(
            call = {
                api.getRatedTVs(language, sessionId, sortBy, page).await()
            }, errorMessage = "Ошибка при получении оцененных сериалов"
        )
    }

    suspend fun getRatedEpisodes(
        language: String?,
        sessionId: String,
        sortBy: String?,
        page: Int?
    ): TVEpisodeResponse? {
        return safeApiCall(
            call = {
                api.getRatedEpisodes(language, sessionId, sortBy, page).await()
            }, errorMessage = "Ошибка при получении оценненых серий"
        )
    }

    suspend fun getWatchlistMovies(
        language: String?,
        sessionId: String,
        sortBy: String?,
        page: Int?
    ): MovieResponse? {
        return safeApiCall(
            call = {
                api.getWatchlistMovies(language, sessionId, sortBy, page).await()
            }, errorMessage = "Ошибка при получении списка фильмов 'Буду смотреть'"
        )
    }

    suspend fun getWatchlistTVs(
        language: String?,
        sessionId: String,
        sortBy: String?,
        page: Int?
    ): TVResponse? {
        return safeApiCall(
            call = {
                api.getWatchlistTVs(language, sessionId, sortBy, page).await()
            }, errorMessage = "Ошибка при получении списка сериалов 'Буду смотреть'"
        )
    }

    suspend fun addToWatchlist(requestAddToWatchlist: RequestAddToWatchlist, sessionId: String): ResponseAPI? {
        return safeApiCall(
            call = {
                api.addToWatchlist(requestAddToWatchlist, sessionId).await()
            },
            errorMessage = "Неудалось добавть в 'Избранное'" +
                    " с значением: ${requestAddToWatchlist.watchlist} " +
                    "с типом: ${requestAddToWatchlist.mediaType} " +
                    "с id: ${requestAddToWatchlist.mediaId} "
        )
    }
}