package com.rodion.moviebox.clean.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.rodion.moviebox.clean.data.repositories.SearchRepository
import com.rodion.moviebox.clean.domain.pagination.SearchDataSourceFactory
import com.rodion.moviebox.clean.presentation.network.NetworkState
import androidx.lifecycle.Transformations.switchMap

class SearchableViewModel(
    repository: SearchRepository
) : BaseViewModel() {

    private val searchDataSource = SearchDataSourceFactory(
        repository = repository,
        scope = ioScope
    )

    val searchResults = LivePagedListBuilder(searchDataSource, pagedConfig()).build()

    val networkState: LiveData<NetworkState>? =
        switchMap(searchDataSource.source) { it.getNetworkState() }

    fun fetchItems(query: String) {
        val search = query.trim()
        if (searchDataSource.getQuery() == search) return
        searchDataSource.updateQuery(search)
    }

    private fun pagedConfig() = PagedList.Config.Builder()
        .setInitialLoadSizeHint(5)
        .setEnablePlaceholders(false)
        .setPageSize(20)
        .build()

    fun refreshFailedRequest() = searchDataSource.getSource()?.retryFailedQuery()

    fun refreshAllList() = searchDataSource.getSource()?.refresh()

    fun getCurrentQuery() = searchDataSource.getQuery()
}
