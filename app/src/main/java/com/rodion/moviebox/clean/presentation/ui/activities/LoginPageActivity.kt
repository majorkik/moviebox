package com.rodion.moviebox.clean.presentation.ui.activities

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.rodion.moviebox.clean.presentation.viewmodels.AuthViewModel
import kotlinx.android.synthetic.main.activity_login.login_btn
import kotlinx.android.synthetic.main.activity_login.login_guest_btn
import kotlinx.android.synthetic.main.activity_login.login_page_webview
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.annotation.TargetApi
import android.os.Build
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.domain.Constants
import kotlin.Deprecated as Deprecated1

class LoginPageActivity : AppCompatActivity() {

    private val authViewModel: AuthViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login_btn.setOnClickListener {
            authViewModel.getRequestToken()
        }

        authViewModel.requestToken.observe(this, Observer { token ->
            if (token.success != null && token.success == true) {
                hideButtons()

            }
        })

        authViewModel.session.observe(this, Observer {
            //todo сохранит sessionid
        })

    }

    private fun hideButtons() {
        login_btn.visibility = GONE
        login_guest_btn.visibility = GONE
        login_page_webview.visibility = VISIBLE
    }

    private fun showButtons() {
        login_btn.visibility = VISIBLE
        login_guest_btn.visibility = VISIBLE
        login_page_webview.visibility = GONE
    }

    private fun configureWebView(requestToken: String){
        login_page_webview.settings.javaScriptEnabled = true
        login_page_webview.webViewClient = WebViewClient()

        login_page_webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(webView: WebView, url: String): Boolean {
                handleUrl(url, requestToken)

                return false
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(
                webView: WebView,
                request: WebResourceRequest
            ): Boolean {
                val uri = request.url.toString()
                handleUrl(uri, requestToken)

                return false
            }
        }

        login_page_webview.loadUrl(Constants.TMDB_AUTH_URL + requestToken)

    }

    private fun handleUrl(url: String, requestToken: String){
        if (url.contains("/allow")) {
            showButtons()
            authViewModel.createSession(requestToken)
        }
    }
}