package com.rodion.moviebox.clean.data.repositories

import android.util.Log
import com.rodion.moviebox.clean.data.models.CollectionResponse
import com.rodion.moviebox.clean.data.models.genre.Genre
import com.rodion.moviebox.clean.data.models.movie.MovieDetails
import com.rodion.moviebox.clean.data.models.movie.MovieResponse
import com.rodion.moviebox.clean.presentation.network.TMDbApiService

class MovieRepository(private val api: TMDbApiService) : BaseRepository() {
    suspend fun getMovieById(
        movieId: Int,
        language: String?,
        appendToResponse: String?,
        imageLanguages: String?
    ): MovieDetails? {
        return safeApiCall(
            call = {
                api.getMovieById(movieId, language, appendToResponse, imageLanguages).await()
            },
            errorMessage = "Ошибка при получении информации о фильме"
        )
    }

    suspend fun getPopularMovies(
        language: String?,
        page: Int?,
        region: String?
    ): MutableList<CollectionResponse.CollectionItem>? {
        val movieResponse = safeApiCall(
            call = { api.getPopularMovies(language, page, region).await() },
            errorMessage = "Ошибка при получения популярных фильмов"
        )

        return movieResponse?.results?.toMutableList()
    }

    suspend fun getTopRatedMovies(
        language: String?,
        page: Int?,
        region: String?
    ): MutableList<CollectionResponse.CollectionItem>? {
        val movieResponse = safeApiCall(
            call = { api.getTopRatedMovies(language, page, region).await() },
            errorMessage = "Ошибка при получении самых популярных фильмов"
        )

        return movieResponse?.results?.toMutableList()
    }

    suspend fun searchMovies(
        language: String?,
        query: String,
        page: Int?,
        includeAdult: Boolean?,
        region: String?,
        year: Int?,
        primaryReleaseYear: Int?
    ): MutableList<MovieResponse.Movie>? {
        val movieResponse = safeApiCall(
            call = {
                api.searchMovies(
                    language,
                    query,
                    page,
                    includeAdult,
                    region,
                    year,
                    primaryReleaseYear
                ).await()
            }, errorMessage = "Ошибка при поиске фильмов"
        )

        return movieResponse?.results?.toMutableList()
    }

    suspend fun getMovieGenres(language: String?): MutableList<Genre>? {
        val movieResponse = safeApiCall(
            call = { api.getMovieGenres(language).await() },
            errorMessage = "Ошибка при получении списка жанров для фильмов"
        )

        return movieResponse?.genres?.toMutableList()
    }
}