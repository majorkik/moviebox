package com.rodion.moviebox.clean.presentation.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.CollectionType
import com.rodion.moviebox.clean.data.models.CollectionType.POPULAR_TV
import com.rodion.moviebox.clean.data.models.MediaType
import com.rodion.moviebox.clean.presentation.adapters.PagedMovieAdapter
import com.rodion.moviebox.clean.presentation.network.NetworkState
import com.rodion.moviebox.clean.presentation.network.NetworkState.FAILED
import com.rodion.moviebox.clean.presentation.network.NetworkState.RUNNING
import com.rodion.moviebox.clean.presentation.network.NetworkState.SUCCESS
import com.rodion.moviebox.clean.presentation.viewmodels.PagingMovieViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import kotlinx.android.synthetic.main.fragment_movie_page.grid_items as recyclerView
import kotlinx.android.synthetic.main.fragment_movie_page.movie_empty_list_button as emptyListButton
import kotlinx.android.synthetic.main.fragment_movie_page.movie_empty_list_image as emptyListImage
import kotlinx.android.synthetic.main.fragment_movie_page.movie_empty_list_progressbar as progressBar
import kotlinx.android.synthetic.main.fragment_movie_page.movie_empty_list_title as emptyListTitle


class TVFragment(private val collectionType: CollectionType) : BasePagedFragment(), PagedMovieAdapter.OnClickListener  {
    private val tvViewModel: PagingMovieViewModel by viewModel{ parametersOf(collectionType) }
    private lateinit var adapter: PagedMovieAdapter

    override fun getLayoutId() = R.layout.fragment_tv_page

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureRecyclerView()
        configureObservables()
        configureOnClick()
    }

    private fun configureOnClick() {
        emptyListButton.setOnClickListener{tvViewModel.refreshAllList()}
    }

    private fun configureRecyclerView() {
        adapter = PagedMovieAdapter(MediaType.TV, this)
        recyclerView.adapter = adapter
    }

    private fun configureObservables() {
        tvViewModel.networkState?.observe(this, Observer { adapter.updateNetworkState(it) })
        tvViewModel.movieResults.observe(this, Observer { adapter.submitList(it) })
    }

    override fun onClickRetry() {
        tvViewModel.refreshFailedRequest()
    }

    override fun whenListIsUpdated(size: Int, networkState: NetworkState?) {
        updateUIWhenLoading(size, networkState)
        updateUIWhenEmptyList(size, networkState)
    }

    private fun updateUIWhenEmptyList(size: Int, networkState: NetworkState?) {
        emptyListImage.visibility = View.GONE
        emptyListButton.visibility = View.GONE
        emptyListTitle.visibility = View.GONE
        if (size == 0) {
            when (networkState) {
                SUCCESS -> {
                    emptyListTitle.text = getString(R.string.no_result_found)
                    emptyListImage.visibility = View.VISIBLE
                    emptyListTitle.visibility = View.VISIBLE
                    emptyListButton.visibility = View.GONE
                }
                FAILED -> {
                    emptyListTitle.text = getString(R.string.technical_error)
                    emptyListImage.visibility = View.VISIBLE
                    emptyListTitle.visibility = View.VISIBLE
                    emptyListButton.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun updateUIWhenLoading(size: Int, networkState: NetworkState?) {
        progressBar.visibility =
            if (size == 0 && networkState == RUNNING) View.VISIBLE else View.GONE
    }

}