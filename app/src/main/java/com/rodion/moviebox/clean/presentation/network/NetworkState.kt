package com.rodion.moviebox.clean.presentation.network

enum class NetworkState {
    RUNNING,
    SUCCESS,
    FAILED
}