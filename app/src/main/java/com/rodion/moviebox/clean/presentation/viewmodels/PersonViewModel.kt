package com.rodion.moviebox.clean.presentation.viewmodels

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.rodion.moviebox.clean.data.models.person.PersonDetails
import com.rodion.moviebox.clean.data.models.person.PersonImagesResponse
import com.rodion.moviebox.clean.data.models.person.PersonPostersResponse
import com.rodion.moviebox.clean.data.repositories.PersonRepository
import com.rodion.moviebox.clean.presentation.network.TMDbApiService
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class PersonViewModel( val personRepository: PersonRepository) : ViewModel() {
    private val parentJob = Job()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default


    private val scope = CoroutineScope(coroutineContext)

    var personDetailsLiveData = MutableLiveData<PersonDetails>()
    var personImagesLiveData = MutableLiveData<MutableList<PersonImagesResponse.ImageDetails>>()
    var personPostersLiveData = MutableLiveData<MutableList<PersonPostersResponse.ImageDetails>>()

    fun fetchPersonDetails(personId: Int,
                           language: String?,
                           appendToResponse: String?){
        scope.launch {
            val personDetails = personRepository.getPersonById(personId, language, appendToResponse)
            personDetailsLiveData.postValue(personDetails)
        }
    }

    fun fetchPersonTaggedImages(
        personId: Int,
        language: String?,
        page: Int
    ){
        scope.launch {
            val personImages = personRepository.getPersonTaggedImages(personId, language, page)
            personImagesLiveData.postValue(personImages)
        }
    }

    fun fetchPersonPosters(
        personId: Int
    ){
        scope.launch {
            val personPosters = personRepository.getPersonPosters(personId)
            personPostersLiveData.postValue(personPosters)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun cancelAllRequests() = coroutineContext.cancel()
}