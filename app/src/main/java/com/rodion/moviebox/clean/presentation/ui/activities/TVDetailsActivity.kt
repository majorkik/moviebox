package com.rodion.moviebox.clean.presentation.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.Cast
import com.rodion.moviebox.clean.data.models.tv.TVDetails
import com.rodion.moviebox.clean.data.models.Video
import com.rodion.moviebox.clean.presentation.adapters.CastAdapter
import com.rodion.moviebox.clean.presentation.adapters.slider.SliderAdapter
import com.rodion.moviebox.clean.presentation.adapters.TVSeasonAdapter
import com.rodion.moviebox.clean.presentation.adapters.trailers.VideosAdapter
import com.rodion.moviebox.clean.presentation.extensions.setMovieLength
import com.rodion.moviebox.clean.presentation.extensions.setStatusBarSupport
import com.rodion.moviebox.clean.presentation.extensions.setTitleCollapsingToolbar
import com.rodion.moviebox.clean.presentation.extensions.setToolbarWithBackButton
import com.rodion.moviebox.clean.presentation.viewmodels.TVViewModel
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator
import kotlinx.android.synthetic.main.activity_movie_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TVDetailsActivity : AppCompatActivity() {

    private var title: String = " "
    private val tvViewModel: TVViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tv_details)

        //statusbar
        setStatusBarSupport()

        //toolbar with button
        setToolbarWithBackButton(R.id.toolbar_tv_details)

        //intent data
        val extras: Bundle? = intent.extras

        setTitleCollapsingToolbar(extras)

        if (extras != null)
            tvViewModel.fetchTVDetails(
                extras.getInt("id"),
                "ru",
                "images,credits,videos",
                "ru,null"
            )

        tvViewModel.tvDetailsLiveData.observe(this, Observer { tv ->
            title = tv.name
            movie_title.text = tv.name
            movie_genres.text = tv.genres.joinToString { it.name }
            movie_short_info.text =
                (tv.firstAirDate + " | " + tv.originalLanguage + " | " + tv.lastAirDate)

            expand_text_view.text = tv.overview

            movie_status.text = tv.status
            original_title.text = tv.originalName
            release_date.setMovieLength(tv.episodeRunTime[0])

            loadBackdropImage(tv.images.backdrops.map { it.filePath }.take(6))
            setCreditsAdapter(tv.credits.casts.take(10))
            setSeasonsAdapter(tv.seasons)
            setVideosAdapter(tv.videos.results)
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun loadBackdropImage(listImages: List<String>) {
        val dotsIndicator = findViewById<WormDotsIndicator>(R.id.worm_dots_indicator)
        val viewPager = findViewById<ViewPager>(R.id.view_pager)
        val adapter =
            SliderAdapter(listImages)

        viewPager.adapter = adapter
        dotsIndicator.setViewPager(viewPager)
    }

    private fun setCreditsAdapter(list: List<Cast>) {
        val recyclerView: RecyclerView = findViewById(R.id.list_credits)
        recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = CastAdapter(list)
    }

    private fun setSeasonsAdapter(seasons: List<TVDetails.Season>) {
        val recyclerView: RecyclerView = findViewById(R.id.list_seasons)
        recyclerView.layoutManager =
            LinearLayoutManager(this)
        recyclerView.adapter = TVSeasonAdapter(seasons)
    }

    private fun setVideosAdapter(list: List<Video>) {
        val recyclerView: RecyclerView = findViewById(R.id.list_videos)
        recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter =
            VideosAdapter(list)
    }
}
