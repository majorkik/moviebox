package com.rodion.moviebox.clean.presentation.adapters.slider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.rodion.moviebox.clean.domain.Constants
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.presentation.extensions.displayImage
import com.stfalcon.imageviewer.StfalconImageViewer

class SliderAdapter(private val backdropImages: List<String>) : PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun getCount(): Int = backdropImages.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater =
            container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater.inflate(R.layout.item_backdrop_slider, null)
        val imageView = view.findViewById(R.id.slider_image) as ImageView
        val viewPager: ViewPager = container as ViewPager

        imageView.displayImage(Constants.TMDB_BACKDROP_SIZE_780 + backdropImages[position])

        viewPager.addView(view, 0)

        imageView.setOnClickListener {
            StfalconImageViewer.Builder<String>(container.context, backdropImages) { view, image ->
                view.displayImage(Constants.TMDB_BACKDROP_SIZE_1280 + image)
            }.withStartPosition(position).show()
        }

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val viewPager = container as ViewPager
        val view = `object` as View
        viewPager.removeView(view)
    }
}