package com.rodion.moviebox.clean.domain.pagination

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.rodion.moviebox.clean.data.models.CollectionResponse
import com.rodion.moviebox.clean.data.models.CollectionResponse.CollectionItem
import com.rodion.moviebox.clean.data.models.CollectionType
import com.rodion.moviebox.clean.data.models.CollectionType.POPULAR_MOVIE
import com.rodion.moviebox.clean.data.models.CollectionType.POPULAR_TV
import com.rodion.moviebox.clean.data.models.CollectionType.TOP_RATED_MOVIE
import com.rodion.moviebox.clean.data.models.CollectionType.TOP_RATED_TV
import com.rodion.moviebox.clean.data.models.movie.MovieResponse
import com.rodion.moviebox.clean.data.repositories.MovieRepository
import com.rodion.moviebox.clean.data.repositories.TVRepository
import com.rodion.moviebox.clean.presentation.network.NetworkState
import com.rodion.moviebox.clean.presentation.network.NetworkState.FAILED
import com.rodion.moviebox.clean.presentation.network.NetworkState.RUNNING
import com.rodion.moviebox.clean.presentation.network.NetworkState.SUCCESS
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MovieDataSource(
    private val movieRepository: MovieRepository,
    private val tvRepository: TVRepository,
    private val collectionType: CollectionType,
    private val scope: CoroutineScope
) : PageKeyedDataSource<Int, CollectionItem>() {
    private var supervisorJob = SupervisorJob()
    private val networkState = MutableLiveData<NetworkState>()
    private var retryQuery: (() -> Any)? = null

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, CollectionItem>
    ) {
        retryQuery = { loadInitial(params, callback) }
        executeQuery(1) {
            callback.onResult(it, null, 2)
        }
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, CollectionItem>
    ) {
        val page = params.key
        retryQuery = { loadAfter(params, callback) }
        executeQuery(page) {
            callback.onResult(it, page + 1)
        }

    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, CollectionItem>
    ) {
    }

    private fun executeQuery(page: Int, callback: (List<CollectionItem>) -> Unit) {
        networkState.postValue(RUNNING)
        scope.launch(getJobErrorHandler() + supervisorJob) {
            val result = getRequestByType(page)
            if(result != null) {
                retryQuery = null
                networkState.postValue(SUCCESS)
                callback(result)
            }else {
                networkState.postValue(FAILED)
            }
        }
    }

    private suspend fun getRequestByType(page: Int): MutableList<CollectionItem>? {
        return when(collectionType){
            POPULAR_MOVIE -> {
                movieRepository.getPopularMovies(null, page, null)
            }
            POPULAR_TV -> {
                tvRepository.getPopularTVs(null, page)
            }
            TOP_RATED_MOVIE -> {
                movieRepository.getTopRatedMovies(null, page, null)
            }
            TOP_RATED_TV -> {
                tvRepository.getTopRatedTVs(null, page)
            }
        }
    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        Log.e(MovieDataSource::class.java.simpleName, "Ошибка: $e")
        networkState.postValue(FAILED)
    }


    override fun invalidate() {
        super.invalidate()
        supervisorJob.cancelChildren()
    }

    fun getNetworkState(): LiveData<NetworkState> = networkState

    fun refresh() = this.invalidate()

    fun retryFailedQuery() {
        val previousQuery = retryQuery
        retryQuery = null
        previousQuery?.invoke()
    }

}