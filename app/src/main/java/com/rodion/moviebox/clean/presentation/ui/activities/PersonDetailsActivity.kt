package com.rodion.moviebox.clean.presentation.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.person.PersonDetails
import com.rodion.moviebox.clean.presentation.adapters.MovieCreditsAdapter
import com.rodion.moviebox.clean.presentation.adapters.slider.SliderAdapter
import com.rodion.moviebox.clean.presentation.adapters.TVCreditsAdapter
import com.rodion.moviebox.clean.presentation.extensions.setStatusBarSupport
import com.rodion.moviebox.clean.presentation.extensions.setTitleCollapsingToolbar
import com.rodion.moviebox.clean.presentation.extensions.setToolbarWithBackButton
import com.rodion.moviebox.clean.presentation.viewmodels.PersonViewModel
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator
import kotlinx.android.synthetic.main.activity_people_details.expand_text_view
import kotlinx.android.synthetic.main.activity_people_details.person_birthday
import kotlinx.android.synthetic.main.activity_people_details.person_gender
import kotlinx.android.synthetic.main.activity_people_details.person_name
import kotlinx.android.synthetic.main.activity_people_details.person_official_site
import kotlinx.android.synthetic.main.activity_people_details.person_original_name
import kotlinx.android.synthetic.main.activity_people_details.person_place_of_birth
import org.koin.androidx.viewmodel.ext.android.viewModel

class PersonDetailsActivity : AppCompatActivity() {

    private val personViewModel: PersonViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_people_details)

        //statusbar
        setStatusBarSupport()

        //toolbar with button
        setToolbarWithBackButton(R.id.toolbar_person_details)

        val extras = intent.extras

        setTitleCollapsingToolbar(extras)

        if (extras != null)
            personViewModel.fetchPersonDetails(
                extras.getInt("id"),
                "ru",
                "movie_credits,tv_credits,images,tagged_images"
            )

        personViewModel.personDetailsLiveData.observe(this, Observer { person ->
            person_name.text = person.name
            person_original_name.text = person.name
            expand_text_view.text = person.biography
            setGenderInfo(person.gender)

            person_birthday.text = person.birthday
            person_official_site.text = person.homepage
            person_place_of_birth.text = person.placeOfBirth

            setMovieCredits(person.movieCredits.cast)
            setTVCredits(person.tvCredits.cast)
            loadBackdropImage(person.backdrops.results.map { it.filePath }.take(6))
        })
    }

    private fun setGenderInfo(genderId: Int) {
        when (genderId) {
            1 -> {
                person_gender.text = "Женский"
            }
            2 -> {
                person_gender.text = "Мужской"
            }
            else -> {
                person_gender.text = "Неизвестно"
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setMovieCredits(cast: List<PersonDetails.MovieCredits.MovieCast>) {
        val recyclerView: RecyclerView = findViewById(R.id.person_movies_credits)
        recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = MovieCreditsAdapter(cast)
    }

    private fun setTVCredits(cast: List<PersonDetails.TVCredits.TVCast>) {
        val recyclerView: RecyclerView = findViewById(R.id.person_tv_credits)
        recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = TVCreditsAdapter(cast)
    }

    private fun loadBackdropImage(listImages: List<String>) {
        val dotsIndicator = findViewById<WormDotsIndicator>(R.id.worm_dots_indicator)
        val viewPager = findViewById<ViewPager>(R.id.view_pager)
        val adapter =
            SliderAdapter(listImages)

        viewPager.adapter = adapter
        dotsIndicator.setViewPager(viewPager)
    }
}