package com.rodion.moviebox.clean.data.models.tv

import com.google.gson.annotations.SerializedName
import com.rodion.moviebox.clean.data.models.Cast
import com.rodion.moviebox.clean.data.models.Crew
import com.rodion.moviebox.clean.data.models.ProductionCompany
import com.rodion.moviebox.clean.data.models.Video
import com.rodion.moviebox.clean.data.models.genre.Genre
import com.rodion.moviebox.clean.data.models.movie.MovieDetails

data class TVDetails(
    @SerializedName("backdrop_path") val backdropPath: String?,
    @SerializedName("created_by") val createdBy: List<CreatedBy>,
    @SerializedName("episode_run_time") val episodeRunTime: List<Int>,
    @SerializedName("first_air_date") val firstAirDate: String,
    @SerializedName("genres") val genres: List<Genre>,
    @SerializedName("homepage") val homepage: String,
    @SerializedName("id") val id: Int,
    @SerializedName("in_production") val inProduction: Boolean,
    @SerializedName("languages") val languages: List<String>,
    @SerializedName("last_air_date") val lastAirDate: String,
    @SerializedName("last_episode_to_air") val episodeToAir: EpisodeToAir,
    @SerializedName("name") val name: String,
    @SerializedName("networks") val networks: List<Network>,
    @SerializedName("next_episode_to_air") val nextEpisodeToAir: EpisodeToAir,
    @SerializedName("number_of_episodes") val numberOfEpisodes: Int,
    @SerializedName("number_of_seasons") val numberOfSeasons: Int,
    @SerializedName("origin_country") val originCountry: List<String>,
    @SerializedName("original_language") val originalLanguage: String,
    @SerializedName("original_name") val originalName: String,
    @SerializedName("overview") val overview: String,
    @SerializedName("popularity") val popularity: Double,
    @SerializedName("poster_path") val posterPath: String?,
    @SerializedName("production_companies") val productionCompanies: List<ProductionCompany>,
    @SerializedName("seasons") val seasons: List<Season>,
    @SerializedName("status") val status: String,
    @SerializedName("type") val type: String,
    @SerializedName("vote_average") val voteAverage: Double,
    @SerializedName("vote_count") val voteCount: Int,

    //append to resppnse
    @SerializedName("images") val images: MovieDetails.Images,
    @SerializedName("credits") val credits: MovieDetails.Credits,
    @SerializedName("videos") val videos: MovieDetails.Videos
) {
    data class CreatedBy(
        @SerializedName("id") val id: Int,
        @SerializedName("credit_id") val creditId: String,
        @SerializedName("name") val name: String,
        @SerializedName("gender") val gender: Int,
        @SerializedName("profile_path") val profilePath: String
    )

    data class EpisodeToAir(
        @SerializedName("air_date") val airDate: String,
        @SerializedName("episode_number") val episodeNumber: Int,
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("overview") val overview: String,
        @SerializedName("production_code") val productionCode: String,
        @SerializedName("season_number") val seasonNumber: Int,
        @SerializedName("show_id") val showId: Int,
        @SerializedName("still_path") val stillPath: String,
        @SerializedName("vote_average") val voteAverage: Double,
        @SerializedName("vote_count") val voteCount: Int
    )

    data class Network(
        @SerializedName("name") val name: String,
        @SerializedName("id") val id: Int,
        @SerializedName("logo_path") val logoPath: String,
        @SerializedName("origin_country") val originCountry: String
    )

    data class Season(
        @SerializedName("air_date") val airDate: String,
        @SerializedName("episode_count") val episodeCount: Int,
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("overview") val overview: String,
        @SerializedName("poster_path") val posterPath: String,
        @SerializedName("season_number") val seasonNumber: Int
    )

    //append to response
    data class Images(
        @SerializedName("id") val id: Int,
        @SerializedName("backdrops") val backdrops: List<ImageDetails>,
        @SerializedName("posters") val posters: List<ImageDetails>
    ) {
        data class ImageDetails(
            @SerializedName("aspect_ratio") val aspectRatio: Double,
            @SerializedName("file_path") val filePath: String,
            @SerializedName("height") val height: Int,
            @SerializedName("iso_631_1") val iso_631_1: String?,
            @SerializedName("vote_average") val voteAverage: Double,
            @SerializedName("vote_count") val voteCount: Int,
            @SerializedName("width") val width: Int
        )
    }

    data class Credits(
        @SerializedName("id")val id: Int,
        @SerializedName("cast")val casts: List<Cast>,
        @SerializedName("crew")val crews: List<Crew>
    )

    data class Videos(
        @SerializedName("results") val results: List<Video>
    )
}