package com.rodion.moviebox.clean.data.models.account

import com.google.gson.annotations.SerializedName

data class ResponseAPI(
    @SerializedName("status_code") val statusCode: Int,
    @SerializedName("status_message") val statusMessage: String
)