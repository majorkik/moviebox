package com.rodion.moviebox.clean.presentation.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.Cast
import com.rodion.moviebox.clean.data.models.Video
import com.rodion.moviebox.clean.presentation.adapters.CastAdapter
import com.rodion.moviebox.clean.presentation.adapters.slider.SliderAdapter
import com.rodion.moviebox.clean.presentation.adapters.trailers.VideosAdapter
import com.rodion.moviebox.clean.presentation.extensions.setStatusBarSupport
import com.rodion.moviebox.clean.presentation.extensions.setTitleCollapsingToolbar
import com.rodion.moviebox.clean.presentation.extensions.setToolbarWithBackButton
import com.rodion.moviebox.clean.presentation.viewmodels.MovieViewModel
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator
import kotlinx.android.synthetic.main.activity_movie_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieDetailsActivity : AppCompatActivity() {

    private var titleToolbar: String = " "
    private val movieViewModel: MovieViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        //statusBar
        setStatusBarSupport()

        //toolbar with button
        setToolbarWithBackButton(R.id.toolbar_movie_details)

        //intent
        val extras = intent.extras

        setTitleCollapsingToolbar(extras)

//        if (extras != null) loadData(extras.getInt("id"))
        if (extras != null)
            movieViewModel.fetchMovieDetails(
                extras.getInt("id"),
                "ru",
                "images,credits,videos",
                "ru,null"
            )

        movieViewModel.movieDetailsLiveData.observe(this, Observer { movie ->
            titleToolbar = movie.title
            movie_title.text = movie.title
            movie_genres.text = movie.genres.joinToString { it.name }
            movie_short_info.text =
                (movie.releaseDate + " | " + movie.originalLanguage + " | " + movie.runtime + "мин")
            expand_text_view.text = movie.overview
            movie_budget.text = movie.budget.toString()
            movie_revenue.text = movie.revenue.toString()
            movie_status.text = movie.status
            original_title.text = movie.originalTitle
            release_date.text = movie.releaseDate

            loadBackdropImage(movie.images.backdrops.map { it.filePath }.take(6))

            setCreditsAdapter(movie.credits.casts.take(10))

            setVideosAdapter(movie.videos.results)
        })

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun loadBackdropImage(listImages: List<String>) {
        val dotsIndicator = findViewById<WormDotsIndicator>(R.id.worm_dots_indicator)
        val viewPager = findViewById<ViewPager>(R.id.view_pager)
        val adapter =
            SliderAdapter(listImages)

        viewPager.adapter = adapter
        dotsIndicator.setViewPager(viewPager)
    }

    private fun setCreditsAdapter(list: List<Cast>) {
        val recyclerView: RecyclerView = findViewById(R.id.list_credits)
        recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = CastAdapter(list)
    }

    private fun setVideosAdapter(list: List<Video>) {
        val recyclerView: RecyclerView = findViewById(R.id.list_videos)
        recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter =
            VideosAdapter(list)
    }

}
