package com.rodion.moviebox.clean.presentation.ui.activities

import android.app.AppComponentFactory
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.CollectionType.POPULAR_MOVIE
import com.rodion.moviebox.clean.data.models.CollectionType.POPULAR_TV
import com.rodion.moviebox.clean.data.models.CollectionType.TOP_RATED_MOVIE
import com.rodion.moviebox.clean.data.models.CollectionType.TOP_RATED_TV
import com.rodion.moviebox.clean.presentation.adapters.tabs.MovieCollectionsPageAdapter
import com.rodion.moviebox.clean.presentation.ui.fragments.MovieFragment
import com.rodion.moviebox.clean.presentation.ui.fragments.TVFragment
import kotlinx.android.synthetic.main.activity_movie_collections.tab_layout
import kotlinx.android.synthetic.main.activity_movie_collections.toolbar
import kotlinx.android.synthetic.main.activity_movie_collections.view_pager

class TVCollectionActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_collections)

        setSupportActionBar(toolbar)
        configureTabLayout()
    }

    private fun configureTabLayout() {
        val adapter = MovieCollectionsPageAdapter(supportFragmentManager)

        adapter.addFragment(TVFragment(POPULAR_TV), "Полурные")
        adapter.addFragment(TVFragment(TOP_RATED_TV), "Самые популярные")
        view_pager.adapter = adapter

        if(adapter.count < 3) {
            tab_layout.tabMode = TabLayout.MODE_FIXED
        }else{
            tab_layout.tabMode = TabLayout.MODE_SCROLLABLE
        }

        tab_layout.setupWithViewPager(view_pager)
        tab_layout.getTabAt(1)?.select()
    }

}