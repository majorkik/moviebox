package com.rodion.moviebox.clean.presentation.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.CollectionResponse
import com.rodion.moviebox.clean.data.models.MediaType
import com.rodion.moviebox.clean.data.models.MediaType.MOVIE
import com.rodion.moviebox.clean.data.models.MediaType.PERSON
import com.rodion.moviebox.clean.data.models.MediaType.TV
import com.rodion.moviebox.clean.domain.Constants
import com.rodion.moviebox.clean.presentation.extensions.displayImage
import com.rodion.moviebox.clean.presentation.ui.activities.MovieDetailsActivity
import com.rodion.moviebox.clean.presentation.ui.activities.TVDetailsActivity
import kotlinx.android.synthetic.main.item_collection.view.collection_item_image
import kotlinx.android.synthetic.main.item_collection.view.parent_layout

class CollectionAdapter(
    private val collectionItems: List<CollectionResponse.CollectionItem>,
    private val mediaType: MediaType
) : RecyclerView.Adapter<CollectionAdapter.CollectionViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CollectionAdapter.CollectionViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_collection, parent, false)

        return CollectionViewHolder(view)
    }

    override fun getItemCount() = collectionItems.size

    override fun onBindViewHolder(holder: CollectionAdapter.CollectionViewHolder, position: Int) {
        holder.collectionImage.displayImage(Constants.TMDB_POSTER_SIZE_92 + collectionItems[position].posterPath)

        holder.item.setOnClickListener {
            val intent = Intent(holder.containerView.context, changeScreen())

            intent.putExtra("id", collectionItems[position].id)

            holder.containerView.context.startActivity(intent)
        }
    }

    private fun changeScreen(): Class<*> {
        return when (mediaType) {
            MOVIE -> {
                MovieDetailsActivity::class.java
            }
            TV -> {
                TVDetailsActivity::class.java
            }
            else -> throw IllegalArgumentException("Неизвестный тип объекта: ${mediaType}")
        }
    }

    class CollectionViewHolder(val containerView: View) : RecyclerView.ViewHolder(containerView) {
        val item: RelativeLayout = itemView.parent_layout as RelativeLayout
        val collectionImage: ImageView = itemView.collection_item_image as ImageView
    }
}