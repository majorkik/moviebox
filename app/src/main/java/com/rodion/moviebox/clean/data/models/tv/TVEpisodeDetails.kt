package com.rodion.moviebox.clean.data.models.tv

import com.google.gson.annotations.SerializedName
import com.rodion.moviebox.clean.data.models.Cast
import com.rodion.moviebox.clean.data.models.Crew

data class TVEpisodeDetails(
    @SerializedName("air_date") val airData: String,
    @SerializedName("crew") val crew: List<Crew>,
    @SerializedName("episode_number") val episodeNumber: Int,
    @SerializedName("guest_stars") val cast: List<Cast>,
    @SerializedName("name") val name: String,
    @SerializedName("overview") val overview: String,
    @SerializedName("id") val id: Int,
    @SerializedName("production_code") val productionCode: String?,
    @SerializedName("season_number") val season_number: Int,
    @SerializedName("still_path") val still_path: String?,
    @SerializedName("vote_average") val vote_average: Double,
    @SerializedName("vote_count") val vote_count: Int
)