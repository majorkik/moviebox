package com.rodion.moviebox.clean.presentation.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.search.MultiSearchResponse.MultiSearchItem
import com.rodion.moviebox.clean.presentation.network.NetworkState
import com.rodion.moviebox.clean.presentation.network.NetworkState.SUCCESS
import com.rodion.moviebox.clean.presentation.ui.activities.MovieDetailsActivity
import com.rodion.moviebox.clean.presentation.ui.activities.PersonDetailsActivity
import com.rodion.moviebox.clean.presentation.ui.activities.TVDetailsActivity
import com.rodion.moviebox.clean.presentation.viewholders.MultiSearchNetworkStateViewHolder
import com.rodion.moviebox.clean.presentation.viewholders.MultiSearchViewHolder
import kotlinx.android.synthetic.main.item_paging_user_network_state.item_paging_network_state_button as retryButton
import kotlinx.android.synthetic.main.item_paging_user_network_state.item_paging_network_state_progress_bar as retryProgressBar
import kotlinx.android.synthetic.main.item_paging_user_network_state.item_paging_network_state_title as retryTitle

class SearchAdapter(
    private val callback: OnClickListener
) : PagedListAdapter<MultiSearchItem, ViewHolder>(diffCallback) {

    private var networkState: NetworkState? = null

    interface OnClickListener {
        fun onClickRetry()
        fun whenListIsUpdated(size: Int, networkState: NetworkState?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.item_multisearch -> MultiSearchViewHolder(
                view
            )
            R.layout.item_paging_user_network_state -> MultiSearchNetworkStateViewHolder(
                view
            )
            else -> throw IllegalArgumentException("Неизвестный тип view: $viewType")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_multisearch -> {
                (holder as MultiSearchViewHolder).bindTo(getItem(position))

                holder.itemView.setOnClickListener {
                    getItem(position)?.let { movie ->
                        val intent = Intent(holder.parent.context, changeScreen(movie.mediaType))

                        intent.putExtra("id", movie.id)

                        holder.parent.context.startActivity(intent)
                    }
                }
            }
            R.layout.item_paging_user_network_state -> {
                (holder as MultiSearchNetworkStateViewHolder).bindTo(
                    networkState,
                    callback
                )
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            R.layout.item_paging_user_network_state
        } else {
            R.layout.item_multisearch
        }
    }

    override fun getItemCount(): Int {
        this.callback.whenListIsUpdated(super.getItemCount(), this.networkState)
        return super.getItemCount()
    }

    private fun hasExtraRow() = networkState != null && networkState != SUCCESS

    fun updateNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    private fun changeScreen(mediaType: String): Class<*> {
        return when (mediaType) {
            "movie" -> {
                MovieDetailsActivity::class.java
            }
            "tv" -> {
                TVDetailsActivity::class.java
            }
            "person" -> {
                PersonDetailsActivity::class.java
            }
            else -> throw IllegalArgumentException("Неизвестный тип обьекта: $mediaType")
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<MultiSearchItem>() {
            override fun areItemsTheSame(
                oldItem: MultiSearchItem,
                newItem: MultiSearchItem
            ): Boolean = oldItem == newItem

            override fun areContentsTheSame(
                oldItem: MultiSearchItem,
                newItem: MultiSearchItem
            ): Boolean = oldItem == newItem

        }
    }
}