package com.rodion.moviebox.clean.presentation.network

import com.rodion.moviebox.clean.data.models.CollectionResponse
import com.rodion.moviebox.clean.data.models.account.AccountDetails
import com.rodion.moviebox.clean.data.models.account.RequestAddToWatchlist
import com.rodion.moviebox.clean.data.models.account.RequestMarkAsFavorite
import com.rodion.moviebox.clean.data.models.account.ResponseAPI
import com.rodion.moviebox.clean.data.models.auth.RequestToken
import com.rodion.moviebox.clean.data.models.auth.RequestTokenResponse
import com.rodion.moviebox.clean.data.models.auth.SessionResponse
import com.rodion.moviebox.clean.data.models.genre.GenreResponse
import com.rodion.moviebox.clean.data.models.movie.MovieDetails
import com.rodion.moviebox.clean.data.models.movie.MovieResponse
import com.rodion.moviebox.clean.data.models.movie.MovieSortBy
import com.rodion.moviebox.clean.data.models.person.PersonDetails
import com.rodion.moviebox.clean.data.models.person.PersonImagesResponse
import com.rodion.moviebox.clean.data.models.person.PersonPostersResponse
import com.rodion.moviebox.clean.data.models.person.PersonResponse
import com.rodion.moviebox.clean.data.models.search.MultiSearchResponse
import com.rodion.moviebox.clean.data.models.tv.TVDetails
import com.rodion.moviebox.clean.data.models.tv.TVEpisodeResponse
import com.rodion.moviebox.clean.data.models.tv.TVResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDbApiService {

    //Movies
    @GET("movie/{movie_id}")
    fun getMovieById(
        @Path("movie_id") movieId: Int,
        @Query("language") language: String?,
        @Query("append_to_response") appendToResponse: String?,
        @Query("include_image_language") imageLanguages: String?
    ): Deferred<Response<MovieDetails>>

    @GET("movie/popular")
    fun getPopularMovies(
        @Query("language") language: String?,
        @Query("page") page: Int?,
        @Query("region") region: String?
    ): Deferred<Response<CollectionResponse>>

    @GET("movie/top_rated")
    fun getTopRatedMovies(
        @Query("language") language: String?,
        @Query("page") page: Int?,
        @Query("region") region: String?
    ): Deferred<Response<CollectionResponse>>
    //TVs
    @GET("tv/{tv_id}")
    fun getTVById(
        @Path("tv_id") tvId: Int,
        @Query("language") language: String?,
        @Query("append_to_response") appendToResponse: String?,
        @Query("include_image_language") imageLanguages: String?
    ): Deferred<Response<TVDetails>>

    @GET("tv/popular")
    fun getPopularTVs(
        @Query("language") language: String?,
        @Query("page") page: Int?
    ): Deferred<Response<CollectionResponse>>


    @GET("tv/top_rated")
    fun getTopRatedTVs(
        @Query("language") language: String?,
        @Query("page") page: Int?
    ): Deferred<Response<CollectionResponse>>

    //Persons
    @GET("person/{person_id}")
    fun getPersonById(
        @Path("person_id") personId: Int,
        @Query("language") language: String?,
        @Query("append_to_response") appendToResponse: String?
    ): Deferred<Response<PersonDetails>>

    @GET("person/{person_id}/tagged_images")
    fun getPersonTaggedImages(
        @Path("person_id") personId: Int,
        @Query("language") language: String?,
        @Query("page") page: Int?
    ): Deferred<Response<PersonImagesResponse>>

    @GET("person/{person_id}/images")
    fun getPersonPosters(
        @Path("person_id") personId: Int
    ): Deferred<Response<PersonPostersResponse>>

    //Genres
    @GET("genre/movie/list")
    fun getMovieGenres(
        @Query("language") language: String?
    ): Deferred<Response<GenreResponse>>

    @GET("genre/tv/list")
    fun getTVGenres(
        @Query("language") language: String?
    ): Deferred<Response<GenreResponse>>
    //Account
    @GET("account")
    fun getAccountDetails(
        @Query("session_id") sessionId: String
    ): Deferred<Response<AccountDetails>>

    @GET("account/{account_id}/favorite/movies")
    fun getFavoriteMovies(
        @Query("language") language: String?,
        @Query("session_id") sessionId: String,
        @Query("sort_by") sortBy: String?,
        @Query("page") page: Int?
    ): Deferred<Response<MovieResponse>>

    @GET("account/{account_id}/favorite/tv")
    fun getFavoriteTVs(
        @Query("language") language: String?,
        @Query("session_id") sessionId: String,
        @Query("sort_by") sortBy: String?,
        @Query("page") page: Int?
    ): Deferred<Response<TVResponse>>

    @POST("account/{account_id}/favorite")
    fun markIsFavorite(
        @Body requestMarkAsFavorite: RequestMarkAsFavorite,
        @Query("session_id") sessionId: String
    ): Deferred<Response<ResponseAPI>>

    @GET("account/{account_id}/rated/movies")
    fun getRatedMovies(
        @Query("language") language: String?,
        @Query("session_id") sessionId: String,
        @Query("sort_by") sortBy: String?,
        @Query("page") page: Int?
    ): Deferred<Response<MovieResponse>>

    @GET("account/{account_id}/rated/tv")
    fun getRatedTVs(
        @Query("language") language: String?,
        @Query("session_id") sessionId: String,
        @Query("sort_by") sortBy: String?,
        @Query("page") page: Int?
    ): Deferred<Response<TVResponse>>

    @GET("account/{account_id}/rated/tv/episodes")
    fun getRatedEpisodes(
        @Query("language") language: String?,
        @Query("session_id") sessionId: String,
        @Query("sort_by") sortBy: String?,
        @Query("page") page: Int?
    ): Deferred<Response<TVEpisodeResponse>>

    @GET("account/{account_id}/watchlist/movies")
    fun getWatchlistMovies(
        @Query("language") language: String?,
        @Query("session_id") sessionId: String,
        @Query("sort_by") sortBy: String?,
        @Query("page") page: Int?
    ): Deferred<Response<MovieResponse>>

    @GET("account/{account_id}/watchlist/tv")
    fun getWatchlistTVs(
        @Query("language") language: String?,
        @Query("session_id") sessionId: String,
        @Query("sort_by") sortBy: String?,
        @Query("page") page: Int?
    ): Deferred<Response<TVResponse>>

    @POST("account/{account_id}/watchlist")
    fun addToWatchlist(
        @Body requestAddToWatchlist: RequestAddToWatchlist,
        @Query("session_id") sessionId: String
    ): Deferred<Response<ResponseAPI>>



    //Search
    @GET("search/movie")
    fun searchMovies(
        @Query("language") language: String?,
        @Query("query") query: String,
        @Query("page") page: Int?,
        @Query("include_adult") includeAdult: Boolean?,
        @Query("region") region: String?,
        @Query("year") year: Int?,
        @Query("primary_release_year") primaryReleaseYear: Int?
    ): Deferred<Response<MovieResponse>>

    @GET("search/tv")
    fun searchTVSeries(
        @Query("language") language: String?,
        @Query("query") query: String,
        @Query("page") page: Int?,
        @Query("first_air_date_year") firstAirDateYear: Int?
    ): Deferred<Response<TVResponse>>

    @GET("search/multi")
    fun multiSearch(
        @Query("language") language: String?,
        @Query("query") query: String,
        @Query("page") page: Int?,
        @Query("include_adult") includeAdult: Boolean?
    ): Deferred<Response<MultiSearchResponse>>

    @GET("search/person")
    fun searchPeoples(
        @Query("language") language: String?,
        @Query("query") query: String,
        @Query("page") page: Int?,
        @Query("include_adult") includeAdult: Boolean?,
        @Query("region") region: String?
    ): Deferred<Response<PersonResponse>>

    //Auth
    @GET("authentication/token/new")
    fun getRequestToken(): Deferred<Response<RequestTokenResponse>>

    @POST("authentication/session/new")
    fun createSession(
        @Body requestToken: RequestToken
    ): Deferred<Response<SessionResponse>>
}