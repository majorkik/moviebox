package com.rodion.moviebox.clean.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.R
import com.rodion.moviebox.clean.data.models.tv.TVDetails
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_tv_season.text_season_name

class TVSeasonAdapter(private val seasons: List<TVDetails.Season>) :
    RecyclerView.Adapter<TVSeasonAdapter.TVSeasonViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TVSeasonViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_tv_season, parent, false)

        return TVSeasonViewHolder(view)
    }

    override fun getItemCount() = seasons.size

    override fun onBindViewHolder(holder: TVSeasonViewHolder, position: Int) {
        holder.titleSeason.text = ("Сезон ${seasons[position].seasonNumber} - ${seasons[position].name}")
    }


    class TVSeasonViewHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {
        val titleSeason = text_season_name as TextView
    }

}