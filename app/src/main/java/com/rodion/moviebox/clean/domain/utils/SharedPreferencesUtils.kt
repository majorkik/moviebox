package com.rodion.moviebox.clean.domain.utils

import android.R.id.edit
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

object SharedPreferencesUtils {
    private const val LOGGED_IN_STATUS = "logged_in_status"
    private const val SESSION_ID = "session_id"
    private const val USERNAME = "username"

    private fun getPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun setLoggedIn(context: Context, loggedIn: Boolean, sessionId: String?, username: String?) {
        val editor = getPreferences(context).edit()
        editor.putBoolean(LOGGED_IN_STATUS, loggedIn)
        editor.putString(SESSION_ID, sessionId)
        editor.putString(USERNAME, username)
        editor.apply()
    }

    fun getLoggedStatus(context: Context): Boolean {
        return getPreferences(context).getBoolean(LOGGED_IN_STATUS, false)
    }

    fun getSessionId(context: Context): String?{
        return getPreferences(context).getString(SESSION_ID, null)
    }

    fun getUsername(context: Context): String? {
        return getPreferences(context).getString(USERNAME, null)
    }
}
