package com.rodion.moviebox.clean

import android.app.Application
import com.rodion.moviebox.clean.data.di.appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

open class MovieBoxApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidLogger(Level.DEBUG)
            androidContext(this@MovieBoxApplication)
            modules(appComponent)
        }
    }
}