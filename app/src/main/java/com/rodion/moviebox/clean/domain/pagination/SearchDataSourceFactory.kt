package com.rodion.moviebox.clean.domain.pagination

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.rodion.moviebox.clean.data.models.search.MultiSearchResponse.MultiSearchItem
import com.rodion.moviebox.clean.data.repositories.SearchRepository
import kotlinx.coroutines.CoroutineScope

class SearchDataSourceFactory (
    private val repository: SearchRepository,
    private var query: String = "",
    private val scope: CoroutineScope
): DataSource.Factory<Int, MultiSearchItem>(){
    val source = MutableLiveData<SearchDataSource>()

    override fun create(): DataSource<Int, MultiSearchItem> {
        val source = SearchDataSource(repository, query, scope)
        this.source.postValue(source)
        return source
    }

    fun getQuery() = query

    fun getSource() = source.value

    fun updateQuery(query: String){
        this.query = query
        getSource()?.refresh()
    }

}