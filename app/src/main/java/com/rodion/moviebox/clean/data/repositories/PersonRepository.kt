package com.rodion.moviebox.clean.data.repositories

import com.rodion.moviebox.clean.data.models.person.PersonDetails
import com.rodion.moviebox.clean.data.models.person.PersonImagesResponse
import com.rodion.moviebox.clean.data.models.person.PersonPostersResponse
import com.rodion.moviebox.clean.presentation.network.TMDbApiService

class PersonRepository(private val api: TMDbApiService) : BaseRepository() {
    suspend fun getPersonById(
        personId: Int,
        language: String?,
        appendToResponse: String?
    ): PersonDetails? {
        return safeApiCall(
            call = {
                api.getPersonById(personId, language, appendToResponse).await()
            },
            errorMessage = "Ошибка при получении информации об актере"
        )
    }

    suspend fun getPersonTaggedImages(
        personId: Int,
        language: String?,
        page: Int?
    ): MutableList<PersonImagesResponse.ImageDetails>? {
        val personResponse = safeApiCall(
            call = {
                api.getPersonTaggedImages(personId, language, page).await()
            },
            errorMessage = "Ошибка при получении списка картинок с актером"
        )

        return personResponse?.results?.toMutableList()
    }

    suspend fun getPersonPosters(
        personId: Int
    ): MutableList<PersonPostersResponse.ImageDetails>?{
        val personResponse = safeApiCall(
            call = {
                api.getPersonPosters(personId).await()
            },
            errorMessage = "Ошибка при получении постеров актера"
        )

        return personResponse?.profiles?.toMutableList()
    }
}