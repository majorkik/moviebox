package com.rodion.moviebox.clean.data.models

enum class CollectionType(val value: String) {
    POPULAR_MOVIE("popular_movie"),
    POPULAR_TV("popular_tv"),
    TOP_RATED_MOVIE("top_rated_movie"),
    TOP_RATED_TV("top_rated_tv")
}