package com.rodion.moviebox.clean.data.models.tv

enum class TVSortBy(val value: String) {
    VOTE_AVERAGE_ASC("vote_average.asc"),
    VOTE_AVERAGE_DESC("vote_average.desc"),
    POPULARITY_ASC("popularity.asc"),
    POPULARITY_DESC("popularity.desc"),
    FIRST_AIR_DATE_ASC("first_air_date.asc"),
    FIRST_AIR_DATE_DESC("first_air_date.desc"),
}