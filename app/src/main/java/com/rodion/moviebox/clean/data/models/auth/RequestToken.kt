package com.rodion.moviebox.clean.data.models.auth

import com.google.gson.annotations.SerializedName

data class RequestToken (
    @SerializedName("request_token") val requestToken: String
)