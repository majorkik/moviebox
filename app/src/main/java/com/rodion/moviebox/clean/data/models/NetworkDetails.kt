package com.rodion.moviebox.clean.data.models

import com.google.gson.annotations.SerializedName

data class NetworkDetails(
    @SerializedName("headquarters") val headquarters: String,
    @SerializedName("homepage") val homepage: String,
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("origin_country") val originCountry: String
)