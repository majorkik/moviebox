package com.rodion.moviebox.clean.presentation.viewmodels

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.rodion.moviebox.clean.data.models.tv.TVDetails
import com.rodion.moviebox.clean.presentation.network.TMDbApiService
import com.rodion.moviebox.clean.data.repositories.TVRepository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class TVViewModel( val tvRepository: TVRepository) : ViewModel() {
    private val parentJob = Job()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)

    var tvDetailsLiveData = MutableLiveData<TVDetails>()

    fun fetchTVDetails(
        tvId: Int,
        language: String?,
        appendToResponse: String?,
        imageLanguages: String?
    ) {
        scope.launch {
            val tvDetails = tvRepository.getTVById(tvId, language, appendToResponse, imageLanguages)
            tvDetailsLiveData.postValue(tvDetails)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun cancelAllReqests() = coroutineContext.cancel()
}