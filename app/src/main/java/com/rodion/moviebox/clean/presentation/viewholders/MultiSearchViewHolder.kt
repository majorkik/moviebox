package com.rodion.moviebox.clean.presentation.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.rodion.moviebox.clean.data.models.search.MultiSearchResponse.MultiSearchItem
import com.rodion.moviebox.clean.domain.Constants
import com.rodion.moviebox.clean.presentation.extensions.displayImage
import kotlinx.android.synthetic.main.item_multisearch.view.imageView as image
import kotlinx.android.synthetic.main.item_multisearch.view.multisearch_title as searchTitle
import kotlinx.android.synthetic.main.item_multisearch.view.multisearch_additional_info as searchAddTitle
import kotlinx.android.synthetic.main.item_multisearch.view.multisearch_type as searchType

class MultiSearchViewHolder(val parent: View) : RecyclerView.ViewHolder(parent) {
    fun bindTo(multiSearchItem: MultiSearchItem?) {
        multiSearchItem?.let {
            when (it.mediaType) {
                "movie" -> {
                    setMovieData(it)
                }
                "tv" -> {
                    setTVData(it)
                }
                "person" -> {
                    setPersonData(it)
                }
            }
        }
    }

    private fun setMovieData(multiSearchItem: MultiSearchItem) {
        itemView.image.displayImage(Constants.TMDB_POSTER_SIZE_185 + multiSearchItem.posterPath)
        itemView.searchTitle.text = multiSearchItem.title
        itemView.searchType.text = multiSearchItem.mediaType
    }

    private fun setPersonData(multiSearchItem: MultiSearchItem) {
        itemView.image.displayImage(Constants.TMDB_POSTER_SIZE_185 + multiSearchItem.profilePath)
        itemView.searchTitle.text = multiSearchItem.name
        itemView.searchType.text = multiSearchItem.mediaType
    }

    private fun setTVData(multiSearchItem: MultiSearchItem) {
        itemView.image.displayImage(Constants.TMDB_PROFILE_SIZE_185 + multiSearchItem.posterPath)
        itemView.searchTitle.text = multiSearchItem.name
        itemView.searchType.text = multiSearchItem.mediaType
    }
}