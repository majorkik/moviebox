package com.rodion.moviebox.clean.presentation.adapters

import android.os.Build
import android.view.View
import androidx.viewpager.widget.ViewPager


class CardsPagerTransformerShift(
    private val baseElevation: Float,
    private val raisingElevation: Int,
    private val smallerScale: Float,
    private val startOffset: Float
): ViewPager.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        val absPosition = Math.abs(position - startOffset)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (absPosition >= 1) {
                page.elevation = baseElevation
                page.scaleY = smallerScale
            } else {
                // This will be during transformation
                page.elevation = (1 - absPosition) * raisingElevation + baseElevation
                page.scaleY = (smallerScale - 1) * absPosition + 1
            }
        }
    }

}