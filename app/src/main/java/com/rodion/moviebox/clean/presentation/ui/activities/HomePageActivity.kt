package com.rodion.moviebox.clean.presentation.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener
import com.rodion.moviebox.R
import com.rodion.moviebox.R.layout
import com.rodion.moviebox.clean.presentation.ui.fragments.HomeFragment
import kotlinx.android.synthetic.main.acivity_home_page.*
import kotlinx.android.synthetic.main.layout_app_bar_main.*

class HomePageActivity : AppCompatActivity(), OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.acivity_home_page)

        supportFragmentManager.beginTransaction()
            .replace(R.id.relativelayout, HomeFragment.newInstance())
            .commit()

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)

        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                setTitle(R.string.menu_main_page)
                supportFragmentManager.beginTransaction()
                    .replace(R.id.relativelayout, HomeFragment.newInstance())
                    .commit()
            }

            R.id.nav_movies -> {
                val intent = Intent(this, MovieCollectionsActivity::class.java)

                startActivity(intent)
            }

            R.id.nav_tv -> {
                val intent = Intent(this, TVCollectionActivity::class.java)

                startActivity(intent)
            }

            R.id.nav_will_watch -> {
                //setTitle(R.string.menu_will_watch)

                val intent = Intent(this, LoginPageActivity::class.java)

                startActivity(intent)

                //Toast.makeText(this, "Выбор элемента Буду смотреть", Toast.LENGTH_SHORT).show()
            }

            R.id.nav_looked -> {
                setTitle(R.string.menu_looked)
                Toast.makeText(this, "Выбор элемента Посмотрел", Toast.LENGTH_SHORT).show()
            }

            R.id.nav_liked -> {
                setTitle(R.string.menu_liked)
                Toast.makeText(this, "Выбор элемента Понравились", Toast.LENGTH_SHORT).show()
            }

            R.id.nav_settings -> {
                setTitle(R.string.menu_settings)
                Toast.makeText(this, "Выбор элемента Настройки", Toast.LENGTH_SHORT).show()
            }

            R.id.nav_help -> {
                setTitle(R.string.menu_help)
                Toast.makeText(this, "Выбор элемента Помощь", Toast.LENGTH_SHORT).show()
            }

            R.id.nav_about -> {
                setTitle(R.string.menu_about)
                Toast.makeText(this, "Выбор элемента О программе", Toast.LENGTH_SHORT).show()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home_page, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.home_page_search_item -> {
                startActivity(Intent(this, SearchableActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
