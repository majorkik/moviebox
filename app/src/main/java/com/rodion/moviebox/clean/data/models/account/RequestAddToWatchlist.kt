package com.rodion.moviebox.clean.data.models.account

import com.google.gson.annotations.SerializedName

data class RequestAddToWatchlist (
    @SerializedName("media_type") val mediaType: String,
    @SerializedName("media_id") val mediaId: Int,
    @SerializedName("watchlist") val watchlist: Boolean
)