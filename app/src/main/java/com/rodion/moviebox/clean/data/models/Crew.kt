package com.rodion.moviebox.clean.data.models

import com.google.gson.annotations.SerializedName

data class Crew(
    @SerializedName("id") val id: Int,
    @SerializedName("credit_id") val creditId: String,
    @SerializedName("name") val name: String,
    @SerializedName("department") val department: String,
    @SerializedName("job") val job: String,
    @SerializedName("profile_path") val profilePath: String?,
    @SerializedName("gender") val gender: Int
)